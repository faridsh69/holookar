<div class="product-card text-center width-200">
	<a href="/holookars/{{$product->id}}">
		<div class="product-image-container">
			<img src="{{ asset($product->base_image() ) }}" class="product-image">
		</div>
		<div class="seperate"></div>
		<p class="product-name">
			{{ $product->first_name }}
			{{ $product->last_name }}
		</p>
		<div class="one-third-seperate"></div>
		<div class="text-right">
			سن: {{ $product->age}}
			<div class="one-third-seperate"></div>
			شهر: {{ $product->city}}
			<div class="one-third-seperate"></div>
			کد کاربر: {{ $product->id }}
		</div>
	</a>
	<div class="one-third-seperate"></div>
	<a href="tel:{{ $constant['phone'] }}" class="btn btn-success btn-block btn-sm">تماس</a>
</div>