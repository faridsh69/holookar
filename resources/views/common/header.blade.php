<meta charset="utf-8">
<title>@yield('title')</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="@yield('keywords')">
<meta name="author" content="farid shahidi">
@if(Request::segment(1) == 'admin')
<meta name="viewport" content="width=device-width, initial-scale=0.6">
@else
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
@endif
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="description" content="@yield('description')">
<meta itemprop="name" content="@yield('title')">
<meta itemprop="description" content="@yield('description')">
<meta itemprop="image" content="@yield('image')">

<meta property="og:url" content="{{ url()->current() }}">
<meta property="og:title" content="@yield('title')">
<meta property="og:description" content="@yield('description')">
<meta property="og:type" content="website">
<meta property="og:locale" content="fa_IR" />
<meta property="og:locale:alternate" content="ar_IR" />
<meta property="og:image" content="@yield('image')">
<meta property="og:site_name" content="{{ url('/') }}">

<meta property="twitter:card" content="summary">
<meta property="twitter:site" content="{{ url()->current() }}">
<meta property="twitter:title" content="@yield('title')">
<meta property="twitter:description" content="@yield('description')">
<meta property="twitter:creator" content="farid shahidi">
<meta property="twitter:image" content="@yield('image')">
<meta property="twitter:domain" content="{{ url('/') }}">

<link rel="canonical" href="{{ url('/') }}">
<link rel='icon' href="{{ asset($constant['favicon']) }}" type='image/png'>


<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/rtl.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome4.7.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/kamadatepicker.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('css/jcrop.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('css/v3.css') }}"> -->

@stack('style')