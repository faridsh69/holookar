<div class="article-box">
    <p class="bold big-size">
    {{ $news->title }} 
    </p>
    <div class="half-seperate"></div>
    <p class="article-content">
    {!! $news->content !!} 
    </p>
</div>