@if(request::segment(1) != 'admin')
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-xs-9">
            <div class="half-seperate"></div>
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-7">
                    <a href="{{ url('/') }}">
                        <span class="hidden-xs">
                        به
                        </span>
                        {{ $constant['name'] }}
                        <span class="hidden-xs">
                        خوش آمدید!
                        </span>
                    </a>
                    <div class="language-box">
                        <a href="/language/fa"><img src="{{ asset('images/iran.png') }}" class="language-icon"></a>
                        <a href="/language/en"><img src="{{ asset('images/englis.png') }}" class="language-icon"></a>
                    </div>
                    @if(empty(Auth::user()))
                        <a href="{{ url('/user/register') }}" class="margin-right-10">
                            <span class="glyphicon glyphicon-user"></span> ثبت نام
                        </a>
                        |
                        <a href="{{ url('/user/login') }}" class=""><span class="glyphicon glyphicon-log-in"></span> ورود</a>
                    @else
                        <a href="{{ url('/admin/profile') }}" class="margin-right-10">
                        <span class="glyphicon glyphicon-user"></span>
                        {{ Auth::user() ? Auth::user()->first_name : ''}} 
                        {{ Auth::user() ? Auth::user()->last_name : ''}}
                        <!-- <label class="label label-default" style="margin:3px;padding: 0px 7px 0px 7px">
                        اعتبار: {{ \Nopaad\Persian::correct(number_format( Auth::user()->credit , 0, '',',')) }} تومان
                        </label> -->
                        @if(\Auth::user()->roles()->count() > 0)
                        <span class="">
                            @foreach(\Auth::user()->roles()->pluck('name') as $role)
                                <small>
                                    <span class="label label-danger">{{ trans('roles.' . $role) }}</span>
                                </small>
                            @endforeach
                        </span>
                        @endif
                        </a>
                    @endif

                </div>
                <div class="col-md-3 col-sm-4 col-xs-5">
                    <ul class="list-inline margin-0 pull-left">
                        <li class="li-no-decoration">
                            <a href="tel:{{ $constant['mobile'] }}" class="a-no-decoration">
                                <i class="fa fa-phone margin-right-5 font-13 color-gray hidden-xs"></i>
                                <span class="">{{ $constant['mobile'] }}</span>
                            </a>
                        </li>
                        <!-- <li class="li-no-decoration visible-xs">
                            <i class="fa fa-search font-13 color-gray"></i>
                        </li> -->
                        <li class="li-no-decoration">
                            <a href="{{ url('/admin/profile') }}" type="button" class="btn btn-xs btn-success visible-xs">
                                هلوکار هستم
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="half-seperate"></div>
            <div class="half-seperate"></div>
            <div class="row hidden-xs">
                <div class="col-lg-2 col-sm-3 col-xs-4">
                    <div class="btn-group">
                        <a href="{{ url('/admin/profile') }}" class="btn btn-success" type="button">
                            <span class="margin-right-5">هلوکار هستم</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                </div>
                <div class="half-seperate"></div>
            </div>
        </div>
        <div class="col-lg-2 col-xs-3">
            <div class="seperate"></div>
            <a class="" href="{{ url('/') }}"> 
                <img src="{{ asset($constant['logo']) }}" alt="{{ $constant['name'] }}" class="header-logo">
            </a>
            <div class="seperate"></div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default background-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <!-- <span class="glyphicon glyphicon-menu-hamburger"></span> -->
                <i class="fa fa-dedent"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="{{ url('/holookars') }}" class="draw">
                        لیست هلوکارها
                    <span class="caret"></span></a>
                </li>
                <!-- <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">مطالب
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/advertise') }}">آگهی ها</a></li>
                        <li><a href="{{ url('/forum') }}">پرسش و پاسخ</a></li>
                        <li><a href="{{ url('/content/article') }}">مقالات</a></li>
                        <li><a href="{{ url('/content/news') }}">اخبار</a></li>
                    </ul>
                </li> -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">صفحات
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach(\App\Models\Page::Active()->get() as $page)
                        <li><a href="{{ url('/content/page/'. $page->id) }}">{{ $page->title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <!-- <li><a href="{{ url('/product/comparison') }}" class="draw">مقایسه</a></li> -->
                <li><a href="javascript:void(0)" data-toggle="modal" data-target="#contact-us-modal" class="draw">تماس با ما</a></li>
            </ul>
        </div>
    </div>
</nav>
@else
@endif
        
@if(Request::segment(1) != 'admin')
<img src="{{ asset('/images/shadow-3.png') }}" width="100%" class="background-nav-shadow">
@else
@endif
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">تماس با {{ $constant['name'] }}</h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center bold">
                    {{ $constant['name'] }}
                </h3>
                
                <h5 style="direction: rtl">
                    تلفن ثابت:
                    {{ $constant['phone'] }}
                </h5>
                <h5>
                    تلفن همراه:
                    {{ $constant['mobile'] }}
                </h5>
                <div class="half-seperate"></div>
                 <h5>
                    فکس:
                    {{ $constant['fax'] }}
                </h5>
                <h5>
                    آدرس:
                    {{ $constant['address'] }}
                </h5>
                <h5>
                    ایمیل:
                    {{ $constant['email'] }}
                </h5>
                <h5>
                    آدرس تلگرام:
                    {{ $constant['telegram'] }}
                </h5>
                 <h5>
                    آدرس اینستاگرام:
                    {{ $constant['instagram'] }}
                </h5>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="about-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">معرفی مرکز</h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center bold">
                    {{ $constant['name'] }}
                </h3>
                <div class="seperate"></div>
                <h5 style="line-height: 180%">
                    {{ $constant['description'] }}
                </h5>
            </div>
        </div>
    </div>
</div>
