<div class="text-center">
	<div>
		<img src="{{ asset($brand->base_image() ) }}" alt="{{ $brand->title }}"  class="brand-index">
		<h4>
			{{ $brand->title }}
		</h4>
	</div>
</div>