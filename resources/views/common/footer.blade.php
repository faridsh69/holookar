@if(Request::segment(1) != 'admin')
<div class="background-footer">
<div class="container">
    <div class="seperate"></div>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-3">
            <h4 class="page-header">
            {{ $constant['name'] }}
            </h4>
        </div>
        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-0 col-md-8 col-lg-9">
            <!-- <div class="seperate"></div> -->
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-4 col-md-3">
            <ul class="list-unstyled">
                <li>
                    <a href="/type/1">معرفی مرکز</a>
                </li>
                <li>
                    <a href="/type/1">خدمات</a>
                </li>
                <li>
                    <a href="/type/1">مقالات</a>
                </li>
                <li>                   
                    <a href="/type/1">پرسش و پاسخ</a>
                </li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
        <div class="col-sm-4 col-md-3">
            <!-- <img id="jzpeoeuksizprgvjsizp" style="cursor:pointer;display: none;" onclick="window.open(&quot;https://logo.samandehi.ir/Verify.aspx?id=78939&amp;p=jyoemcsipfvlxlaopfvl&quot;, &quot;Popup&quot;,&quot;toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=450, height=630, top=30&quot;)" alt="logo-samandehi" src="https://logo.samandehi.ir/logo.aspx?id=78939&amp;p=yndtaqgwbsiyqftibsiy" > -->
            <img src="{{ asset('/upload/images/enamad.png') }}">
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-4 col-md-3">
            <ul class="list-unstyled">
                <li>                    
                    <a href="/type/1"></a>
                </li>
                <li>                    
                    <a href="/type/1"></a>
                </li>
                <li>                    
                    <a href="/type/1"></a>
                </li>
                <li>                
                    <a href="/type/1"></a>
                </li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>

    	<div class="col-md-12 col-md-3 text-right">
            <div>{{ $constant['name']}} در شبکه‌های اجتماعی</div>
            <div class="half-seperate"></div>
            <div class="social">
                <a href="https://www.facebook.com/{{ $constant['facebook'] or $constant['name'] }}">  </a>
                <a href="https://telegram.me/{{ $constant['telegram'] }}">  </a>
                <a href="https://www.instagram.com/{{ $constant['instagram'] }}">  </a>
                <a href="https://www.twitter.com/{{ $constant['twitter'] or $constant['name'] }}">  </a>
                <a href="https://www.plus.google.com/{{ $constant['google_plus'] or $constant['name'] }}">  </a>
                <a href="https://www.linkden.com/{{ $constant['linkden'] or $constant['name'] }}">  </a>
            </div>
            <div class="one-third-seperate"></div>
            <p>کلیه حقوق این سایت متعلق به {{ $constant['name']}}
            می‌باشد.</p>
            <p style="color: #444;font-size: 95%">
                نویسنده سایت :<a href="http://rotbeyek.ir/cv">رتبه یک</a>
            </p>
        </div>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>
</div>
@endif