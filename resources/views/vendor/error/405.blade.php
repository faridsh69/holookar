@extends('layout.master-error')
@section('title', 'این متد موجود نیست')
@section('container')
  	<p><b>405.</b> <ins>متد اتصال به این آدرس اشتباه است.</ins></p>
  	<p>فایل web.php را بررسی کنید و به get/post دقت کنید.</p>
  	<img src="{{ asset('/images/405.png') }}" class="icon">
@endsection