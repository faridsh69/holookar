
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 text-center">         
        <h1 class="bold page-header hug-size">
            {{ $constant['name'] }}
        </h1>
        <h3>
            {{ $constant['description'] }}
        </h3>
    </div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<!-- begin products slider -->
<div class="row text-center background-gradiant-1">
    <div class="col-xs-12">
        <h4>هلوکارها...</h4>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <!-- <div class="col-xs-12"> -->
    	<div class="owl-carousel" id="new-product-slider">
            @each('common.product-box', $new_products, 'product')
        </div>
	<!-- </div> -->
</div>
<!-- end products slider -->
<div class="seperate"></div>
<div class="row">
    <div class="col-sm-6">
        <div class="owl-carousel" id="right-slider">
            @each('common.slider-box', $baners_right_slider, 'baner')
        </div>
    </div> 
    <div class="col-sm-6">
        <div class="owl-carousel" id="left-slider">
            @each('common.slider-box', $baners_left_slider, 'baner')
        </div>
    </div>
</div>
<div class="seperate"></div>

<div class="seperate"></div>
<div class="seperate"></div>
<!-- begin news slider -->
<div class="row">
    <div class="col-xs-12">
        <div class="row text-center background-gradiant-3">
            <div class="col-xs-12">
                <h4>اخبار </h4>
            </div>
        </div>
        <div class="seperate"></div>
        <div class="row">
            <div class="owl-carousel" id="news-slider">
                @each('common.news-box', $newses, 'news')
            </div>
        </div>
    </div>
</div>
<!-- end news slider -->
<div class="seperate"></div>
<div class="seperate"></div>
<!-- begin news slider -->

<!-- end news slider -->
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
