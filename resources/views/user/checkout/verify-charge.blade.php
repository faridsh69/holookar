@extends('layout.master')
@section('container')

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12 text-center">
		<h2>
			بازگشت از پرداخت
		</h2>
	</div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12">
		@if($code == 3 || $code == 2)
		<div class="alert alert-danger">
			<div class="seperate"></div>
			<h3 class="text-center">
				پرداخت شما با موفقیت انجام نشد.
			</h3>
			<div class="seperate"></div>
			<ul>
				<li>
					شما می توانید با مراجعه به صفحه <a href="/admin/profile">مجددا پرداخت خود را انجام دهید</a>.
				</li>
				<li>
					درصورت هرگونه مشکلی می توانید با شماره تلفن {{\App\Http\Controllers\Controller::PHONE}} 
				    تماس حاصل نمایید.
				</li>
			</ul>
		</div>
		@elseif($code == 1)
		<div class="alert alert-success">
			<div class="seperate"></div>
			<h3 class="text-center">
				پرداخت شما با موفقیت انجام شد.
			</h3>
			<div class="seperate"></div>
			<ul>
				<li>
					حساب کاربری شما شارژ شد.
				</li>
				<li>
					هم اکنون {{ Auth::user()->credit }} تومان در حساب خود دارید.
				</li>
				<li>
					می توانید با حساب خود از سایت خرید نمایید.
				</li>
				<li>
					در صورت بروز هر گونه مشکلی می توانید با تماس با تیم پشتیبانی ما مشکل را رفع نمایید.
				</li>
			</ul>
			<div class="seperate"></div>
		</div>
		@endif
	</div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
@endsection