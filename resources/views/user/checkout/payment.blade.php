@extends('layout.master')
@section('title', 'روش پرداخت')
@section('container')
<div class="seperate"></div>
<div class="row text-center steps-row-css">
    <div class="col-xs-4">
        <a href="{{ url('/checkout/address') }}">
            <img src="{{ asset('/images/step1.png') }}" alt="مرحله ۱">
            <div class="half-seperate"></div>
            <p>
            <span class="glyphicon glyphicon-check hidden-xs"></span>
            انتخاب آدرس 
            </p>
        </a>
    </div>
    <div class="col-xs-4">
    	<a href="{{ url('/checkout/shipping') }}">
	        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۲">
	        <div class="half-seperate"></div>
	        <p>
	        	<span class="glyphicon glyphicon-check hidden-xs"></span>
	            انتخاب روش ارسال 
	        </p>
	    </a>
    </div>
    <div class="col-xs-4">
    	<a href="{{ url('/checkout/payment') }}">
	        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۳">
	        <div class="half-seperate"></div>
	        <p class="bold">
	            انتخاب روش پرداخت
	        </p>
	    </a>
    </div>
</div>
<div class="row">
	<div class="col-xs-12 text-center">
		<h3>
			انتخاب روش پرداخت: 
		</h3>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="simple-cart">
			پرداخت اینترنتی از درگاه امن بانک:
			<div class="seperate"></div>
			<a href="/checkout/payment/online" class="btn btn-block btn-success">
				پرداخت اینترنتی
			</a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="simple-cart">
			اگر حداقل <b> یک بار پرداخت آنلاین </b> داشته باشید می توانید هزینه را در محل پرداخت کنید.
			<div class="seperate"></div>
				<a href="{{ url('/checkout/payment/local') }}" class="btn btn-block btn-primary">پرداخت در محل</a>
		</div>
	</div>
</div>
@include('common.factor-box')


@endsection
