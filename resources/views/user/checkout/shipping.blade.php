@extends('layout.master')
@section('title', 'روش ارسال')
@section('container')
<div class="seperate"></div>
<div class="row text-center steps-row-css">
    <div class="col-xs-4">
        <a href="{{ url('/checkout/address') }}">
            <img src="{{ asset('/images/step1.png') }}" alt="مرحله ۱">
            <div class="half-seperate"></div>
            <p>
            <span class="glyphicon glyphicon-check hidden-xs"></span>
            انتخاب آدرس 
            </p>
        </a>
    </div>
    <div class="col-xs-4">
    	<a href="{{ url('/checkout/shipping') }}">
	        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۲">
	        <div class="half-seperate"></div>
	        <p class="bold">
	            انتخاب روش ارسال 
	        </p>
	    </a>
    </div>
    <div class="col-xs-4">
        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۳">
        <div class="half-seperate"></div>
        <p>
            انتخاب روش پرداخت
        </p>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12 text-center">
		<h3>
			انتخاب روش ارسال: 
		</h3>
	</div>
</div>
<form action="{{ url('/checkout/shipping') }}" method="post">
	<div class="row">
		<input type="hidden" name="factor_id" value=" {{ $factor->id }}">
		{{ csrf_field() }}
		@foreach($shippings as $shipping)
		<div class="col-xs-4" >
			<div class="simple-cart">
				<div class="radio-style">
					<div class="radio-button">
						<input type="radio" name="shipping" id="{{ $shipping->id }}" value="{{ $shipping->title }}" checked="">
						<label for="{{ $shipping->id }}">
					    	<span class="radio">
					    		<p class="bold"  for="{{ $shipping->id }}">
									{{ $shipping->title }}
								</p>
								<div class="help-block">
									{!! $shipping->description !!}
								</div>
								قیمت:
								{{ number_format($shipping->value) }}
								تومان
					    	</span>
				    	</label>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-success btn-block btn-lg">
				ادامه خرید
	  			<i class="fa fa-arrow-circle-o-left"></i>
			</button>
		</div>
	</div>
	<div class="seperate"></div>
	@include('common.factor-box')
	<div class="row">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-success btn-block btn-lg">
				ادامه خرید
	  			<i class="fa fa-arrow-circle-o-left"></i>
			</button>
		</div>
	</div>
</form>
@endsection
