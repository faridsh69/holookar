@extends('layout.master')
@section('title', 'آدرس دهی')
@section('container')
<div class="seperate"></div>
<div class="row text-center steps-row-css">
    <div class="col-xs-4">
        <a href="{{ url('/checkout/address') }}">
            <img src="{{ asset('/images/step1.png') }}" alt="مرحله ۱">
            <div class="half-seperate"></div>
            <p class="bold">
            انتخاب آدرس 
            </p>
        </a>
    </div>
    <div class="col-xs-4">
        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۲">
        <div class="half-seperate"></div>
        <p >
            انتخاب روش ارسال 
        </p>
    </div>
    <div class="col-xs-4">
        <img src="{{ asset('/images/step2.png') }}" alt="مرحله ۳">
        <div class="half-seperate"></div>
        <p>
            انتخاب روش پرداخت
        </p>
    </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="seperate"></div>
	 	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul class="list-unstyled">
                        <li>{{ Session::get('alert-' . $msg) }}</li>
                    </ul>
                </div>
            @endif
        @endforeach
        @if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif
	</div>	
</div>
<choose-address :provinces_json="{{ json_encode( \Config::get('constants.provinces') ) }}"></choose-address>
@include('admin.profile.create-address')
<div class="seperate"></div>
@each('common.basket-box', [$basket], 'basket')

@endsection
@push('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaTGuyJD5pQKp9i2zkyhg5NJ76RH3vLlA&callback=initMap" type="text/javascript"></script>
<script src="{{ asset('/js/google-map-marker.js') }}"></script>
@endpush
