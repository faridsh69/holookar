@extends('layout.master')
@section('title', 'هلوکارها')
@section('description', 'هلوکارها' )
@section('image', $constant['logo'])
@section('container')
<div class="row">
	<div class="col-xs-12">
		<h2 class="page-header text-center">
			هلوکارها
		</h2>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<!-- <form class="form-inline" method="GET">
		  	<div class="form-group">
		    	<label for="name">عنوان یا متن:</label>
		   	 	<input type="text" class="form-control input-sm" id="name" name="name" value="{{
		   	 	Request::input('name') }}">
		  	</div>
		  	<div class="form-group">
		    	<label for="category">دسته بندی: </label>
		    	<select class="form-control" name="category">
					<option value="">همه</option>
					@foreach($categories as $category)
						<option value="{{ $category->id }}"
						{{ Request::input('category') == $category->id ? "selected" : ""}} >
							{{ $category->title }}
						</option>
					@endforeach
				</select>
		  	</div>
		  	<button type="submit" class="btn btn-default">جستجو</button>
		</form> -->
	</div>
</div>
<!-- <hr> -->
<!-- <div class="seperate"></div> -->

<div class="row">
	@each('common.product-box', $articles, 'product')
</div>
<div class="text-center">
{{ $articles->links() }}
</div>
<div class="seperate"></div>
@endsection