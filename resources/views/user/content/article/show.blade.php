@extends('layout.master')
@section('title', 'هلوکار'. $article->title)
@section('description', 'هلوکار' )
@section('image', $constant['logo'])
@section('container')
<div class="row">
	<div class="col-xs-12">
        <div class="half-seperate"></div>
			<div class="panel-boddy">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="seperate"></div>
						<h4 class="page-header">
						هلوکار کد {{ $article->id }} -
						{{ $article->first_name }}
						{{ $article->last_name }}
						</h4>
						@if($article->image_id)
						<img src="{{ $article->avatar->src }}" class="img-responsive">
						@else
						<div class="seperate"></div>
						<div class="text-center">
						تصویر آپلود نشده است!
						</div>
						@endif
						<div class="half-seperate"></div>
						<div class="bold">
							
							سن: 
							{{ $article->age }}
							<div class="one-third-seperate"></div>
							
						</div>
						<div class="half-seperate"></div>
						<div class="bold">
							تحصیلات: 
							{{ $article->tahsilat }}
							<div class="one-third-seperate"></div>
							
						</div>
						<div class="half-seperate"></div>
						<div class="bold">
							شهر: 
							{{ $article->city }}
							<div class="one-third-seperate"></div>
							
						</div>
						<div class="half-seperate"></div>
						<div>
							<label>توضیحات:</label>
							{{ $article->description}}
						</div>
						<div class="seperate"></div>
						شماره تماس:
						<a href="tel:{{ $constant['phone'] }}" class="btn btn-success">
							تماس با هلوکار
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')



@endpush