@extends('layout.master')
@section('title', 'خبر'. $news->title)
@section('description', 'خبر' )
@section('image', $constant['logo'])
@section('container')
<div class="row">
	<div class="col-xs-12">
        <div class="half-seperate"></div>
			<div class="panel-boddy">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="seperate"></div>
						خبر شماره {{ $news->id }}
						@if($news->image_id)
						<img src="{{ $news->image->src }}" class="img-responsive">
						@else
						<div class="seperate"></div>
						<!-- <div class="text-center">
						تصویر آپلود نشده است!
						</div> -->
						@endif
						<div class="half-seperate"></div>
						<div class="bold">
							
							عنوان: 
							{{ $news->title }}
							<div class="one-third-seperate"></div>
							
						</div>
						<div class="half-seperate"></div>
						<div>
							<label>دسته بندی:</label>
							{{ $news->category ? $news->category->title : 'ندارد'}}
						</div>
						<div class="half-seperate"></div>
						تاریخ:
-						{{ \Nopaad\jDate::forge( $news->updated_at )->format(' %Y/%m/%d') }}
-						{{ \Nopaad\jDate::forge( $news->updated_at )->format(' %H:%M:%S') }}
						<hr>
						<div>
							<label>متن خبر:</label>
							{!! $news->content !!}
						</div>
						<div class="seperate"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('script')



@endpush