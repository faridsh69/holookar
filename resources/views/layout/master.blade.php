<!DOCTYPE html>
<html lang="{{ Lang::locale() }}" dir="{{ Lang::locale() == 'fa' ? 'rtl' : 'ltr' }}">
	<head>
		@include('common.header')
	</head>
	<body>
		<div id="vue_id">
		@include('common.navbar')
			<div class="container-fluid background-container-fluid">
				@yield('fluid-container')
			</div>
			<div class="container">
				<div class="background-container">
					@yield('container')
				</div>
			</div>
		</div>
		@include('common.footer')
		@include('common.script')
		@stack('script')
	</body>
</html>