@extends('admin.dashboard')
@section('title', 'تنظیمات')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
        @endforeach
       
    	<div class="half-seperate"></div>
        <div class="half-seperate"></div>
		<div class="panel panel-default">
			<div class="panel-heading">
				تنظیمات
			</div>
			<div class="half-seperate"></div>
			<div class="col-xs-12">
			    	<span class="margin-right-10">
			    	</span>
	        </div>
			<div class="reponsive-table">
				<table class="table table-striped">
				<thead>
				<tr>
					<th>
						شماره
					</th>
					<th>
						عنوان
					</th>
					<th>
						مقدار
					</th>
					<th>
						توضیحات
					</th>
					
					<th>
						عملیات
					</th>
				</tr>
				</thead>
				<tbody>
					@if( count($settings) == 0 )
					<tr>
						<td colspan="10">
							<div class="alert">
								موردی یافت نشد !
							</div>
						</td>
					</tr>
					@endif
					@foreach($settings as $setting)
					<tr>
						<td class="text-center width-50px" >
							{{ $setting->id }}
						</td>
						<td>
							{{ $setting->key }}
						</td>
						<td width="50%">
							{{ $setting->value }}
						</td>
						<td>
							{{ $setting->description }}
						</td>
						<td class="width-80px">
							<a href="/admin/manage/setting/{{ $setting->id }}">
								<span class="glyphicon glyphicon-eye-open"></span>
								مشاهده
							</a>
							<div class="one-third-seperate"></div>
							<a href="/admin/manage/setting/{{ $setting->id }}/edit">
								<span class="glyphicon glyphicon-pencil"></span>
								ویرایش
							</a>							
						</td>
					</tr>
					@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="text-center">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<passport-clients></passport-clients>
		<passport-authorized-clients></passport-authorized-clients>
		<passport-personal-access-tokens></passport-personal-access-tokens>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4 class=""> راهنمای رنگ های سیستم </h4>
		<div class="simple-cart">
			<a href="#" class="btn btn-block color-not-seen">
				فاکتور مشاهده نشده است. هر چه سریعتر دیده شود
			</a>
		</div>
		<div class="simple-cart">
			<a href="#" class="btn btn-block color-danger">
				فاکتور باید پیگیری شود
			</a>
		</div>
		<div class="simple-cart">
			<a href="#" class="btn btn-block color-success">
				به سرانجام رسیدن فاکتور
			</a>
		</div>
		<div class="simple-cart">
			<a href="#" class="btn btn-block color-not-active">
				در حال خرید
			</a>
		</div>
	</div>
</div>
@endsection
@push('script')



@endpush