@extends('layout.master')
@section('title', 'داشبورد')
@section('fluid-container')
<div class="admin">
	<div class="box-nav">
		<div class="box-nav-inner">
			<div class="items">
				<div class="item">
					<div class="title">
			      		<a href="{{ url('/') }}" class="bold">
							{{ $constant['name'] }}
						</a>

						<!-- <a href="{{ url('/admin/dashboard') }}" class="bold ">
							<span class="glyphicon glyphicon-cog"></span>
							داشبورد
						</a> -->
					</div>
					<div class="sub-item">
						@can('category_manager')
						<a href="{{ url('/admin/manage/category') }}" class=" {{ Request::segment(3) == 'category' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-filter"></span>دسته بندی‌ها</a>
						@endcan			
						@can('content_manager')
						<a href="{{ url('/admin/manage/content/article') }}" class="{{ Request::segment(4) == 'article' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-book"></span>مقالات</a>
						<a href="{{ url('/admin/manage/content/news') }}" class="{{ Request::segment(4) == 'news' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-book"></span>اخبار</a>
						<a href="{{ url('/admin/manage/content/page') }}" class="{{ Request::segment(4) == 'page' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-duplicate"></span>صفحات</a>	
						<a href="{{ url('/admin/manage/baner') }}" class="{{ Request::segment(3) == 'baner' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-duplicate"></span>بنر ها</a>			
						@endcan

						@can('general_manager')
						<a href="{{ url('/admin/manage/user') }}" class="{{ Request::segment(3) == 'user' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-user"></span>مدیریت کاربران</a>
						<a href="{{ url('/admin/manage/setting') }}" class=" {{ Request::segment(3) == 'setting' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-cog"></span> تنظیمات</a>
						<a href="{{ url('/admin/manage/role') }}" class="{{ Request::segment(3) == 'role' ? 'selected':'' }}">
							<span class="glyphicon glyphicon-eye-open"></span>مدیریت دسترسی ها</a>
						@endcan
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-container">
		<div class="content">
			<nav class="navbar navbar-default no-margin-bottom">
			  	<div class="container-fluid">
				    <ul class="nav navbar-nav">
				      	<!-- <li>
				      		<a href="{{ url('/') }}" class="active">
							{{ $constant['name'] }}
							</a>
						</li> -->
						<li><a href="{{ url('/admin/profile') }}" class="{{ Request::segment(2) == 'profile' ? 'selected':'' }}"><span class="glyphicon glyphicon-user"></span>
						اطلاعات رزومه هلوکار </a>
						</li>
						
				      	<li>
				      		<a href="{{ url('/admin/logout') }}" style="color: red">
							<span class="glyphicon glyphicon-off"></span> خروج</a>
						</li>
						<li>
							<div class="half-seperate"></div>
							<div class="one-third-seperate"></div>
							{{ Auth::user() ? Auth::user()->first_name : ''}} 
	                        {{ Auth::user() ? Auth::user()->last_name : ''}}
	                        <span>(امتیاز:{{ Auth::user()->rate }})</span>
	                        @if(\Auth::user()->roles()->count() > 0)
	                        <span>
	                            @foreach(\Auth::user()->roles()->pluck('name') as $role)
	                                <small>
	                                    <span class="label label-danger">{{ trans('roles.' . $role) }}</span>
	                                </small>
	                            @endforeach
	                        </span>
	                        @endif
						</li>
				    </ul>
			  	</div>
			</nav>
			@yield('content')
			<div style="height:10px"></div>
		</div>
	</div>
</div>

@endsection
@push('script')
<script type="text/javascript">
var acc = document.getElementsByClassName("item-title");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

</script>
@endpush