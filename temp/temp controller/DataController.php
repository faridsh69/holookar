<?php

namespace App\Http\Controllers;

class DataController extends Controller
{
	public function getSaleDaily($key)
	{
		$output = [];
		$sale_daily = [
			[
				'name' => 'فروشنده',
				'data' => []
			],
			[
				'name' => 'لانتوری',
				'data' => []
			],
			[
				'name' => 'بارکد',
				'data' => []
			]
		];

		// data = [ [$time , $value] , [] ...];
		for ($i = 0; $i < 100; $i++) {
			$time = strtotime('90 days ago') * 1000 + ($i * 86400000);
			if ($i < 30) {
				$data1 = 90 + $i * 2 + rand(-1, 40);
				$data2 = $i * 3 + rand(1, 33);
				$data3 = 120 + rand(1, 33);
			} else if ($i < 50) {
				$data1 = 150 + rand(1, 26);
				$data2 = 90 + rand(-1, 23);
				$data3 = 260 - $i * 5 + rand(1, 23);
			} else {
				$data1 = 300 - $i * 3 + rand(1, 36);
				$data2 = 250 - $i * 3 + rand(1, 33);
				$data3 = 0;
				if ($data2 < 0) {
					$data2 = 0;
				}
			}
			array_push($sale_daily[0]['data'], [$time, $data1 * 1000000]);
			array_push($sale_daily[1]['data'], [$time, $data2 * 1000000]);
			array_push($sale_daily[2]['data'], [$time, $data3 * 1000000]);
		}
		foreach ($sale_daily as $row => $item) {
			if ($key == $item['name']) {
				$output = $item;
			}
		}

		return $output;
	}

	public function getSaleTotal($key)
	{
		$output = [];
		$sale_daily[0] = $this->getSaleDaily('فروشنده');
		$sale_daily[1] = $this->getSaleDaily('لانتوری');
		$sale_daily[2] = $this->getSaleDaily('بارکد');
		$sale_total = [
			[
				'name' => 'فروشنده',
				'data' => []
			],
			[
				'name' => 'لانتوری',
				'data' => []
			],
			[
				'name' => 'بارکد',
				'data' => []
			]
		];
		$data1 = [0];
		$data2 = [0];
		$data3 = [0];
		for ($i = 1; $i < 100; $i++) {
			$time = strtotime('90 days ago') * 1000 + ($i * 86400000);
			$data1[$i] = $sale_daily[0]['data'][$i][1] + $data1[$i - 1];
			$data2[$i] = $sale_daily[1]['data'][$i][1] + $data2[$i - 1];
			$data3[$i] = $sale_daily[2]['data'][$i][1] + $data3[$i - 1];
			array_push($sale_total[0]['data'], [$time, $data1[$i]]);
			array_push($sale_total[1]['data'], [$time, $data2[$i]]);
			array_push($sale_total[2]['data'], [$time, $data3[$i]]);
		}
		foreach ($sale_total as $row => $item) {
			if ($key == $item['name']) {
				$output = $item;
			}
		}

		return $output;
	}

	public function getComparisonCinema()
	{
		$cinema_list = [
			['title' => 'پردیس سینمایی آزادی'],
			['title' => 'سینما اریکه ایرانیان'],
			['title' => 'سینما استقلال'],
			['title' => 'سینما آفریقا'],
			['title' => 'سینما ایران'],
			['title' => 'سینما بهمن'],
			['title' => 'سینما پارس'],
			['title' => 'پردیس سینمایی چارسو']
		];
		$comparison_cinema = [];
		foreach ($cinema_list as $cinema) {
			$data = [1, 2, 3];
			$cinema_data = ['name' => $cinema['title'], 'data' => $data];
			array_push($comparison_cinema, $cinema_data);
		}
		return $comparison_cinema;
	}

	public function getComparisonTheater()
	{
		$comparison_theater =
			[
				['سالن شماره یک', 2114320000],
				['سالن شماره دو', 1180440000],
				['سالن شماره سه', 1463840000],
				['سالن شماره چهار', 1320980000],
			];

		return $comparison_theater;
	}

	public function getComparisonDaily()
	{
		$comparison_daily = [
			[
				'name' => 'فروشنده',
				'y' => 56
			],
			[
				'name' => 'لانتوری',
				'y' => 44
			]
		];

		return $comparison_daily;
	}

	public function getComparisonTotal()
	{
		$comparison_total = [
			[
				'name' => 'فروشنده',
				'y' => 73
			],
			[
				'name' => 'لانتوری',
				'y' => 27
			]
		];

		return $comparison_total;
	}
}
