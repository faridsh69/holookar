<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapClient;

class ShopController extends Controller
{
			// ->whereHas('subCategory', function ($q) use ($filterId) {
  //               $q->whereHas('categoryGroup', function ($q) use ($filterId) {
  //                   $q->where('category_id', $filterId);
  //               });
  //           })->get();


    public function getCategory($id)
    {
        $category = \App\Models\Category::where('id',$id)->where('type','Forum')->first();
        if($category)
        {
            $Forums = \App\Models\Forum::where('category_id',$id)->where('status',1)->get();

            return view('Forum.category')->withForums($Forums);
        }
        else
        {
            return redirect('/');
        }
    }

    public function getSearch($name)
    {
        // $output = \Cache::remember('Forum', 21600, function () {
        // return \App\Models\Forum::select('id','name')->get();
        // });
        $i = 1 ;
        $Forums = \App\Models\Forum::select('id','name')->get();
        if (strlen($q)>0) {
            $hint="";
            foreach($Forums as $Forum)
            {
                if (stristr($Forum['name'],$q)) {
                    if ($hint=="") {
                        $hint="<ul><li><a href='/Forum/".
                            $Forum['id'] .
                            "-" .
                            $Forum['name'] .
                            "' target='_self'>" .
                            $Forum['name'] . "</a></li>";
                    } else {
                        $i ++ ;
                        if($i > 8){
                            return $hint;
                        }
                        $hint=$hint . "<li><a href='/Forum/" .
                            $Forum['id'] .
                            "-" .
                            $Forum['name'] .
                            "' target='_self'>" .
                            $Forum['name'] . "</a></li>";
                    }
                }
            }
        }
        return $hint;
        $out = "<ul>
            <li><a href='/type/1'>ظروف یکبار مصرف یک</a></li>
            <li><a href='/type/2'>ظروف یکبار مصرف دو</a></li>
            <li><a href='/type/3'>ظروف یکبار مصرف سه</a></li>
            <li><a href='/type/4'>ظروف یکبار مصرف چهار</a></li>
        </ul>";
    }

    public function postComment(Request $request)
    {
        \Validator::make($request->all(), [
            'comment' => 'required',
        ])->validate();
        \App\Models\Comment::create(['user_id' => \Auth::id(), 'comment' => $request['comment'] ]);
        $request->session()->flash('alert-success', 'ممنون از نظردهی شما. نظر شما را حتما با دقت مطالعه می کنیم.');

        return redirect()->back();
    }

    public function getPaymentWithCredit($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPaymentWithCredit : ordder_id ' . $id );
            return redirect('/');
        }
        if(\Auth::user()->credit > $order->price){
            $order->status = "paycredit";
            $order->save();
            $user = \Auth::user();
            $user->credit = $user->credit - $order->price;
            $user->save();
            \Log::info('با اعتبارش پرداخت می کند order_id: ' . $id . ' by user_id: '. \Auth::id() );
            return view('verify')->withCode(1)->withOrder($order);
        }else{
            return view('verify')->withCode(3)->withOrder($order);
        }
    }

    public function getPaymentInLocation($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPaymentInLocation : ordder_id ' . $id );
            return redirect('/');
        }
        if(\App\Models\Order::where('user_id',\Auth::user()->id )->where('status','prepare')->count() > 0 )
        {
            $order = \App\Models\Order::where('id', $id)->where('address_id','!=','')->first();
            $order->status = "willpay";
            $order->save();
            \Log::info('در محل پرداخت می کند order_id: ' . $id . ' by user_id: '. \Auth::id() );
            return view('verify')->withCode(4)->withOrder($order);
        }else{
            return view('verify')->withCode(3)->withOrder($order);
        }
    }

    public function getVerify()
    {        // risky
        $payment = \App\Models\Payment::where('user_id', \Auth::id())->orderBy('id','dsc')->first();
        $order = $payment->order;
        $MerchantID = self::MERCHANT_CODE;
        $Amount = $order->price;
        $Authority = $_GET['Authority'];

        if ($_GET['Status'] == 'OK') {
            $client = new \SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl',array('encoding' => 'UTF-8')); 
            $result = $client->PaymentVerification(
                [
                'MerchantID' => $MerchantID,
                'Authority' => $Authority,
                'Amount'   => $Amount,
                ]
            );
            if ($result->Status == 100) {
                \Log::info('payment good done : by payment_id: '. $payment->id . ' invN : ' .$result->RefID);
                $code = 1;
                $order->status = 'paid';
                $payment->description = $result->RefID;
                $payment->status = 1;
                $payment->Invoice_number = (int)$Authority;
                
                // vase saheb restoran sms bezan
            } else {
                \Log::warning('payment has error : by payment_id: '. $payment->id . ' status : ' .$result->Status);
                $code = 2;
                $payment->Invoice_number = (int)$Authority;
                $payment->description = self::_getErrorZarrinpal($result->Status);
            }
        } else {
            \Log::warning('user canceled: by payment_id: '. $payment->id );
            $code = 3;
            $payment->Invoice_number = (int)$Authority;
            $payment->description = 'کاربر کنسل کرده';
        }
        $payment->save();
        $order->save();
        
        return view('verify')->withCode($code)->withOrder($order);
    }

  
    public function getPayment($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPayment : ordder_id ' . $id );
            return redirect('/');
        }

        if($order->price < 1000){
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        }

        $payment = \App\Models\Payment::create([
            'user_id' => \Auth::id(),
            'order_id' => $id,
            'user_ip' => \Request::ip(),
            'user_agent' => \Request::header('User-Agent'),
            'amount' => $order->price,
            'Invoice_date' => date('now'),
            'description' => 'در حال ورود به بانک',
            'status' => 0,
            ]);
            \Log::info('در حال ورود به بانک با order_id: ' . $id . ' by user_id: '. \Auth::id() 
            . ' with payment_id: ' . $payment->id );
        
        $MerchantID = self::MERCHANT_CODE;           
        $Amount = $order->price; 
        $Description = 'سفارش از ' . self::NAME;  
        $Email = \Auth::user()->email; 
        $Mobile = \Auth::user()->phone; 
        $CallbackURL = url('verify'); 
          
        // URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
        $client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
          
        $result = $client->PaymentRequest(
            array(
                'MerchantID'   => $MerchantID,
                'Amount'   => $Amount,
                'Description'   => $Description,
                'Email'   => $Email,
                'Mobile'   => $Mobile,
                'CallbackURL'   => $CallbackURL
            )
        );
          
        //Redirect to URL You can do it also by creating a form
        if($result->Status == 100)
        {
            $payment->Invoice_number = $result->Authority;
            $payment->save();
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);exit;
        } else {
            $payment->description = 'اطلاعات بانک تو سیستم غلطه';
            $payment->save();
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',$result->Status);
            \Log::warning(' "اطلاعات بانک اشتباه" رخ داده است. ',' کد خطا: '.$result->Status );
        }
    }

    public function getCheckout($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getCheckout : ordder_id ' . $id );
            return redirect('/');
        }

        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);

        $price = 1;
        return view('checkout')->withOrder($order)->withProducts($products);
    }

	public function postAddress($id,Request $request)
    {
        \Validator::make($request->all(), [
            'address' => 'required',
        ])->validate();

        \App\Models\Address::firstOrCreate([
            'name' => $request['address'],
            'user_id' => \Auth::id()
        ]);
        $request->session()->flash('alert-success', 'آدرس شما با موفقیت ذخیره شد.');
        \Log::info('address was entered with name: '. $request['address'] . ' by user_id: ' . \Auth::id());

        return redirect()->back();
    }

    public function getAddress($id)
    {   

        $order = \App\Models\Order::where('id', $id)->whereIn('status',['select','address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getAddress : ordder_id ' . $id );
            return redirect('/');
        }
        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);
        if(\Auth::id())
        {
            if(!$order->user_id)
            {
                $order->user_id = \Auth::id();
            }
            elseif( $order->user_id != \Auth::id() )
            {
                return redirect('/');
            }
            $price = ( $products->sum('price')* (100 - self::OFF) / 100 ) + self::PEYK;
            $order->price = $price;
            $order->status = "address";
            $order->save();
        }

        return view('address')->withOrder($order)->withProducts($products);
    }

    public function getProductType($title)
    {   
        $id = explode(".", $title)[0];
        if(!is_numeric($id) ){
            return redirect('/');
        }
        $restaurant = \App\Models\Restaurant::where('id',$id)->first();
        if($restaurant)
        {
            $foods = \App\Models\Food::where('restaurant_id',$id)->get();
            if( $restaurant->start_time > date('H') || $restaurant->end_time < date('H') )
            {
                $restaurant->order = 0;
            } else {
                $restaurant->order = 1;
            }

            try 
            {
               \App\Models\RestaurantSeen::create(['restaurant_id' => $id]);
            } 
            catch (Exception $e) {}
            return view('restaurant')->withFoods($foods)->withRestaurant($restaurant);
        }
        else
        {
            return redirect('/');
        }
    }

    public function getIndex()
    {
        return view('home');
    }
}
