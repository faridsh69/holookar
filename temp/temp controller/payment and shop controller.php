public function getAddress()
    {   
        $products = 1;
        // return view('user.basket');

        // $order = \App\Models\Order::where('id', $id)->whereIn('status',['select','address','checkout','failed'])->first();
        // if(!$order){
        //     \Log::warning('order not found with getAddress : ordder_id ' . $id );
        //     return redirect('/');
        // }
        
        // $products = [];
        // foreach(json_decode($order->products) as $product_id)
        // {
        //     $products[] = \App\Models\Product::where('id', $product_id)->first();
        // }
        // $products = collect($products);
        // if(\Auth::id())
        // {
        //     if(!$order->user_id)
        //     {
        //         $order->user_id = \Auth::id();
        //     }
        //     elseif( $order->user_id != \Auth::id() )
        //     {
        //         return redirect('/');
        //     }
        //     $price = ( $products->sum('price')* (100 - self::OFF) / 100 ) + self::PEYK;
        //     $order->price = $price;
        //     $order->status = "address";
        //     $order->save();
        // }else{
        //     if($order->user_id){
        //         return redirect('/');
        //     }
        // }

        return view('address');
    }

    public function postAddress(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
        ])->validate();
        \App\Models\Address::firstOrCreate([
            'name' => $request['address'],
            'postal_code' => $request['postcode'],
            'phone' => $request['phone'],
            'sabet' => $request['sabet'],
            'reciever' => $request['name'],
            'user_id' => \Auth::id(),
        ]);
        return redirect()->back();
    }

    public function getShipping()
    {
    }

    public function getPayment()
    {
//        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
//        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
//        if(!$order){
//            \Log::warning('order not found with getPayment : ordder_id ' . $id );
//            return redirect('/');
//        }
//
//        if($order->price < 1000){
//            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
//            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
//        }
//
//        $payment = \App\Models\Payment::create([
//            'user_id' => \Auth::id(),
//            'order_id' => $id,
//            'user_ip' => \Request::ip(),
//            'user_agent' => \Request::header('User-Agent'),
//            'amount' => $order->price,
//            'Invoice_date' => date('now'),
//            'description' => 'در حال ورود به بانک',
//            'status' => 0,
//            ]);
//            \Log::info('در حال ورود به بانک با order_id: ' . $id . ' by user_id: '. \Auth::id()
//            . ' with payment_id: ' . $payment->id );
//
//        $MerchantID = self::MERCHANT_CODE;
//        $Amount = $order->price;
//        $Description = 'سفارش از ' . self::NAME;
//        $Email = \Auth::user()->email;
//        $Mobile = \Auth::user()->phone;
//        $CallbackURL = url('verify');
//
//        // URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
//        $client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8'));
//
//        $result = $client->PaymentRequest(
//            array(
//                'MerchantID'   => $MerchantID,
//                'Amount'   => $Amount,
//                'Description'   => $Description,
//                'Email'   => $Email,
//                'Mobile'   => $Mobile,
//                'CallbackURL'   => $CallbackURL
//            )
//        );
//
//        //Redirect to URL You can do it also by creating a form
//        if($result->Status == 100)
//        {
//            $payment->Invoice_number = $result->Authority;
//            $payment->save();
//            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);exit;
//        } else {
//            $payment->description = 'اطلاعات بانک تو سیستم غلطه';
//            $payment->save();
//            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',$result->Status);
//            \Log::warning(' "اطلاعات بانک اشتباه" رخ داده است. ',' کد خطا: '.$result->Status );
//        }
    }

    
    public function getVerifyPayment()
    {
    //     // risky
    //     $payment = \App\Models\Payment::where('user_id', \Auth::id())->orderBy('id','dsc')->first();
    //     $order = $payment->order;
    //     $MerchantID = self::MERCHANT_CODE;
    //     $Amount = $order->price;
    //     $Authority = $_GET['Authority'];

    //     if ($_GET['Status'] == 'OK') {
    //         $client = new \SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl',array('encoding' => 'UTF-8')); 
    //         $result = $client->PaymentVerification(
    //             [
    //             'MerchantID' => $MerchantID,
    //             'Authority' => $Authority,
    //             'Amount'   => $Amount,
    //             ]
    //         );
    //         if ($result->Status == 100) {
    //             \Log::info('payment good done : by payment_id: '. $payment->id . ' invN : ' .$result->RefID);
    //             $code = 1;
    //             $order->status = 'paid';
    //             $payment->description = $result->RefID;
    //             $payment->status = 1;
    //             $payment->Invoice_number = (int)$Authority;
                
    //             // vase saheb restoran sms bezan
    //         } else {
    //             \Log::warning('payment has error : by payment_id: '. $payment->id . ' status : ' .$result->Status);
    //             $code = 2;
    //             $payment->Invoice_number = (int)$Authority;
    //             $payment->description = self::_getErrorZarrinpal($result->Status);
    //         }
    //     } else {
    //         \Log::warning('user canceled: by payment_id: '. $payment->id );
    //         $code = 3;
    //         $payment->Invoice_number = (int)$Authority;
    //         $payment->description = 'کاربر کنسل کرده';
    //     }
    //     $payment->save();
    //     $order->save();
        
    //     return view('verify')->withCode($code)->withOrder($order);
    }


    public function getCheckout($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getCheckout : ordder_id ' . $id );
            return redirect('/');
        }

        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);

        $price = 1;
        return view('checkout')->withOrder($order)->withProducts($products);
    }   

    
    public function getNotifyOrder($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['paid','paycredit','willpay'])->first();
        if($order){
            $this->_sms_kharidkarde(\Auth::user()->phone);
            return 'send';
        }else{
            \Log::warning('order not found with : ordder_id ' . $id );
            return 'not paid';
        }
    }

    private function _sms_kharidkarde($user_phone)
    {
        $user = \App\Models\User::where('phone',$user_phone)->first();
        $user->notify(new KharidKonande());

        ini_set("soap.wsdl_cache_enabled", "0");
        $sms_client = new \SoapClient(
            'http://payamak-service.ir/SendService.svc?wsdl'
            , array('encoding'=>'UTF-8'));
        try 
        {
            $x = $sms_client->SendSMS([ 
                'userName' => self::SMS_USER,
                'password' => self::SMS_PASS,
                'fromNumber' => self::SMS_PHONE,
                'toNumbers' => [$user_phone],
                'messageContent' => 'با تشکر از خرید شما از ' . self::NAME . "\n" . ' شماره فاکتور خود را در پنل سفارشاتتان می توانید ببینید.',
                'isFlash' => false,
                'recId' => [],
                // 'status' => []
                ]);
            \Log::info('sms با متن خریدار غذا فرستاده شد به '.$user_phone.' با کد: '. $x->SendSMSResult);
        } 
        catch (Exception $e) 
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    private function _getErrorZarrinpal($status)
    {
        switch ($status) {
        case '-1':
            return 'اطلاعات ارسال شده ناقص است.';
            break;
        case '-2':
            return 'آی پی یا مرچنت کد پذیرنده صحیح نیست';
            break;
        case '-3':
            return 'با توجه به محدودیت های شاپرک امکان پرداخت با رقم درخواست شده میسر نمی باشد.';
            break;
        case '-4':
            return 'سطح تایید پذیرنده پایین تر از صطح نقره ای است.';
            break;
        case '-11':
            return 'درخواست مورد نظر یافت نشد.';
            break;
        case '-12':
            return 'امکان ویرایش درخواست میسر نمی باشد.';
            break;
        case '-21':
            return 'هیچ نوع عملیات مالی برای این تراکنش یافت نشد.';
            break;
        case '-22':
            return 'تراکنش نا موفق می باشد.';
            break;
        case '-33':
            return 'رقم تراکنش با رقم پرداخت شده مطابقت ندارد.';
            break;
        case '-34':
            return 'سقف تقسیم تراکنش از لحاظ تعداد با رقم عبور نموده است.';
            break;
        case '-40':
            return 'اجازه دسترسی به متد مربوطه وجود ندارد.';
            break;
        case '-41':
            return 'اطلاعات ارسال شده مربوط به AdditionalData غیر معتر می باشد.';
            break;
        case '-42':
            return 'مدت زمان معتبر طول عمر شناسه پرداخت بین ۳۰ دقیقه تا ۴۰ روز می باشد.';
            break;
        case '-54':
            return 'درخواست مورد نظر آرشیو شده است.';
            break;
        case '100':
            return 'عملیات با موفقیت انجام گردیده است.';
            break;
        case '101':
            return 'عملیات پرداخت موفق بوده و قبلا Payment Verification تراکنش انجام شده است';
            break;
        default:
            return 1000;
            break;
        };
    }