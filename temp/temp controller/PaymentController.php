<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = \App\Models\Payment::where('user_id', \Auth::id())->orderBy('id', 'desc')->Paginate(self::PAGE_SIZE);

        return view('admin.payment.index')->withPayments($payments);
    }

    public function show($id)
    {
        $payment = \App\Models\Payment::where('id',$id)->first();
        return view('admin.payment')->withPayment($payment);
    }
}
