@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" action="" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-danger">
			<div class="panel-heading">	ویرایش سفارش</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
					<th width="40">شناسه</th>
					<td>{{$order->id}}</td>
				</tr>
				<tr>
					<th style="min-width: 40px;max-width: 90px">کاربر</th>
					<td>{{$order->user->phone}}</td>
				</tr>
				<tr>
					<th style="min-width: 40px;max-width: 60px">آدرس</th>
					<td>{{$order->address->name}}</td>
				</tr>
				<tr>			
					<th style="min-width: 40px;max-width: 60px">توضیحات</th>
					<td>{{$order->description}}</td>
				</tr>
				<tr>
					<th>تاریخ</th>
					<td>{{ \Nopaad\jDate::forge( $order->updated_at )->format(' %H:%M:%S') }}
						<br>
						{{ \Nopaad\jDate::forge( $order->updated_at )->format(' %Y/%m/%d') }}
					</td>
				</tr>
				<tr>
					<th>وضعیت</th>
					<td>{{ trans('statuses.' . $order->status) }}</td>
				</tr>
				<tr>
					<th>تخفیف</th>
					<td>{{ \App\Http\Controllers\Controller::OFF }} %</td>
				</tr>
				<tr>
					<th>مبلغ</th>
					<td><input name="price" value="{{$order->price}}"> تومان</td>
				</tr>
				<tr>
					<td style="min-width: 240px;max-width: 260px">غذاها</td>
					<td>
						@foreach( $order->uniqe_products as $product)
						<div>
							<button class="btn btn-xs btn-danger">-</button>
							<label class="label label-success" style="font-size: 85%">
								{{ \Nopaad\Persian::correct( $product->count() ) }} عدد</label> 
							{{ $product[0]->name }}
							<small>
								({{ \Nopaad\Persian::correct( number_format( $product[0]->price * $product->count() , 0, '',',') )  }} تومان)
							</small>
						</div>
						<div class="half-seperate"></div>
						@endforeach
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<button type="submit" class="btn btn-primary btn-block">ویرایش سفارش</button>
					</td>
				</tr>
			</table>			
        	</div>
			</div>
		</form>
	</div>
</div>
<div class="seperate"></div>
@endsection