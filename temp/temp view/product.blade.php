@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="post" action="/admin/product" id="form">
		{{ csrf_field() }}
		<div class="panel {{ Request::segment(4) == 'edit' ? 'panel-info' :'panel-default'}} panel-default">
		<div class="panel-heading">
			@if(Request::segment(3) == 'edit')
			ویرایش محصول: 
			{{ $product->name }}
			<a class="btn btn-success btn-sm" href="/admin/product">افزودن محصول جدید</a>
			@else
			افزودن محصولات
			@endif
			@if( isset($product) )
				<input type="hidden" name="id" value="{{ $product->id }}">
			@endif
		</div>
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">نام محصول</td>
					<td><input type="text" name="name" class="form-control"
					value="{{  $product->name  or old('name')  }}" required></td>
				</tr>
				<tr>
					<td>قیمت</td>
					<td><input type="number" name="price" class="form-control"
					value="{{ $product->price or old('price') }}" required>
					</td>
				</tr>
				<tr>
					<td>دسته بندی</td>
					<td>
					<select class="form-control" name="type_id" required>
						<option value="">بی مادر</option>
						@foreach(\App\Models\Type::get() as $type)
						<option value="{{ $type->id }}"
						{{ ( isset($product) ? $product->type_id : 1 ) == $type->id ? "selected" : ""}} > 
							{{ $type->type ? $type->type->name.' >' : ''  }}  {{ $type->name }}
						</option>
						@endforeach
					</select>
					</td>
				</tr>
				<tr>
					<td>بسته بندی</td>
					<td><input type="text" name="bastebandi" class="form-control"
					value="{{ $product->bastebandi or old('bastebandi') }}" required></td>
				</tr>
				<tr>
					<td>تعداد محصول در هر دسته</td>
					<td><input type="number" name="toharbastebandi" class="form-control"
					value="{{ $product->toharbastebandi or old('toharbastebandi') }}" required></td>
				</tr>
				<tr>
					<td>موجودی</td>
					<td><input type="number" name="mojodi" class="form-control"
					value="{{ $product->mojodi or old('mojodi') }}"></td>
				</tr>
				<tr>
					<td>توضیحات</td>
					<td><textarea type="text" name="description" class="form-control"
					>{{ $product->description or old('description') }}</textarea></td>
				</tr>
				<tr>
					<td>رنگ</td>
					<td><input type="text" name="rang" class="form-control"
					value="{{ $product->rang or old('rang') }}"></td>
				</tr>
				<tr>
					<td>جنس محصول</td>
					<td><input type="text" name="jens" class="form-control"
					value="{{ $product->jens or old('jens') }}"></td>
				</tr>
				<tr>
					<td>بارکد</td>
					<td><input type="text" name="barcode" class="form-control"
					value="{{ $product->barcode or old('barcode') }}"></td>
				</tr>
				<tr>
					<td>سازنده</td>
					<td><input type="text" name="sazande" class="form-control"
					value="{{ $product->sazande or old('sazande') }}"></td>
				</tr>
				<tr>
					<td>گارانتی</td>
					<td><input type="text" name="garanti" class="form-control"
					value="{{ $product->garanti or old('garanti') }}"></td>
				</tr>
				<tr>
					<td>ابعاد</td>
					<td><input type="text" name="size" class="form-control"
					value="{{ $product->size or old('size') }}" ></td>
				</tr>
				<tr>
					<td>کد</td>
					<td><input type="text" name="code" class="form-control"
					value="{{ $product->code or old('code') }}"></td>
				</tr>
				<tr>
					<td>وزن</td>
					<td><input type="text" name="vazn" class="form-control"
					value="{{ $product->vazn or old('vazn') }}"></td>
				</tr>
				<tr>
					<td>سال ساخت</td>
					<td><input type="number" name="year" class="form-control"
					value="{{ $product->year or old('year') }}"></td>
				</tr>
				<tr>
					<td>کاربرد</td>
					<td><input type="text" name="applicant" class="form-control"
					value="{{ $product->applicant or old('applicant') }}"></td>
				</tr>
				<tr>
					<td>آماده برای نمایش در سایت</td>
					<td>
					<div class="radio-style">
					<div class="radio-button">
				    	<input type="radio" id="yes" name="ready" value="1" checked="{{ $product->ready or old('ready') == 1 }}">
				    	<label for="yes"><span class="radio">بله</span> </label>
				    </div>
				    <div class="radio-button">
				    	<input type="radio" id="no" name="ready" value="0">
				    	<label for="no"><span class="radio">خیر</span> </label>
				    </div>
					</div>
					</td>
				</tr>
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر اثر حداکثر ۳۰۰ کلوبایت باشد.</div>
					</td>
					<td>
						<input id="file" type="file" accept='image/*' />
						<div class="half-seperate"></div>
						<button id="cropbutton" type="button" class="btn btn-info btn-xs">برش عکس</button>
						<button id="rotatebutton" type="button" class="btn btn-info btn-xs">چرخش</button>
						<button id="hflipbutton" type="button" class="btn btn-info btn-xs">آینه افقی</button>
						<button id="vflipbutton" type="button" class="btn btn-info btn-xs">آینه عمودی</button>
						<div class="half-seperate"></div>
						<div id="views"></div>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($product))
						عکس سابق
						@if($product->image_id)
						<img src="/storage/product/{{$product->image_id}}-{{$product->image->name}}"  class="img-responsive img-thumbnail">
						@else
						ندارد
						@endif
						@endif
						<p>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>		
				</tr>
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block">ذخیره اطلاعات</button>
		</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">
			محصولات
		</div>
		<div class="reponsive-table">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
				توضیحات 
				</th>
				<th style="width: 30%;">
				تصویر
				</th>
				<th width="100px">
				ویرایش و حذف
				</th>
			</tr>
			</thead>
			<tbody>
			@foreach($products as $product)
			<tr>
				<td>
			        <div class="row product-details">
					    <div class="col-xs-12">
					        <h4 class="page-header text-center">
					            {{ $product->name }}					           
					        </h4>
				            <h6 class="col-md-4 col-sm-6">
				                دسته بندی: 
				                @if($product->type->type)
				                    {{ $product->type->type->name }} >
				                @endif
				                {{ $product->type->name }}
				            </h6>
				            
						    <h6 class="col-md-3 col-sm-6">
						        بسته بندی: {{ $product->bastebandi }}
						    </h6>
						    <h6 class="col-md-2 col-sm-6">
						        تعداد: {{ $product->toharbastebandi }}
						    </h6>
				            <h6 class="col-md-3 col-sm-6">
						        قیمت: {{ $product->price }} تومان
						    </h6>
					    </div>
					    <div class="seperate"></div>    
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        موجودی: {{ $product->mojodi }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        رنگ: {{ $product->rang }}
					    </div>
					     <div class="col-lg-3 col-md-4 col-sm-6">
					        جنس: {{ $product->jens }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        بارکد: {{ $product->barcode }}
					    </div>
					    
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        سازنده: {{ $product->sazande }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        گارانتی: {{ $product->garanti }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        ابعاد: {{ $product->size }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        کد محصول: {{ $product->code }} 
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        وزن : {{ $product->vazn }} 
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        کاربرد: {{ $product->applicant }}
					    </div>
					    <div class="col-lg-3 col-md-4 col-sm-6">
					        سال ساخت: {{ $product->year }}
					    </div>
					    <div class="half-seperate"></div>
					    <div class="col-xs-12">
					        توضیحات:{{ $product->description }}
					    </div> 
					</div>
			    </div>
				</td>
				<td>
					@if($product->image)
					<img src="/storage/product/{{ $product->image->id }}-{{ $product->image->name}}" 
					style="max-width: 100%;max-height: 280px;">
					@endif
				</td>
				<td>
					<a class="btn btn-info btn-sm" href="/admin/product/edit/{{ $product->id }}">
					ویرایش
					</a>
					<div class="half-seperate"></div>
					@if($product->ready == 1)
					<a class="btn btn-danger btn-sm" href="/admin/product/remove/{{ $product->id }}">
						غیرفعال
					</a>
					@else
					<a class="btn btn-success btn-sm" href="/admin/product/active/{{$product->id}}">
						فعال سازی
					</a>
					@endif					
				</td>
			</tr>
			@endforeach
			</tbody>
			</table>
		</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="text-center">
			{{ $products->links() }}
		</div>
	</div>
</div>
@endsection
@push('script')
<link rel="stylesheet" type="text/css" href="/css/jcrop.css">

<script src="/js/jcrop.js"></script>
<script src="/js/jcrop.main.js"></script>
@endpush