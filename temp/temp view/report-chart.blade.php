@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" action="/admin/charge-credit" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-default">
			<div class="panel-heading">	داشبورد</div>
			<div class="row">
				<div class="col-sm-12">
					<sale-daily></sale-daily>
				</div>
				<hr>
				<div class="col-sm-12">
					<sale-total></sale-total>
				</div>
				<hr>	
				<div class="col-sm-12">
					<customer-detail></customer-detail>
				</div>
				<hr>			
				<div class="col-sm-12">
					<comparison-cinema></comparison-cinema>
				</div>
				<hr>
				<div class="col-sm-12">
					<last-year></last-year>
				</div>
				<hr>
				<div class="col-sm-12">
					<sale-annual></sale-annual>
				</div>
				<hr>
				<div class="col-sm-12">
					<comparison-daily></comparison-daily>
				</div>
				<hr>
				<div class="col-sm-12">
					<comparison-theater></comparison-theater>
				</div>
				<hr>
				<div class="col-sm-12">
					<comparison-total></comparison-total>
				</div>
				<hr>							
			</div>
            <!-- <div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
					<td width="190px">مبلغ مورد نظر به تومان:</td>
					<td>
					<input type="number" name="charge-credit" class="form-control">
					<div class="help-block">
						مبلغ را به تومان وارد نمایید و بزرگتر از ۱۰۰۰ باشد.
					</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<button type="submit" class="btn btn-primary btn-block">پرداخت</button>
					</td>
				</tr>
			</table>			
        	</div> -->
			</div>
		</form>
	</div>
</div>
<div class="seperate"></div>
@endsection
@push('script')

<script src="/public/js/chart/highstock.src.js"></script>
<!-- <script src="https://code.highcharts.com/highcharts.src.js"></script> -->
<script src="/public/js/chart/highcharts-more.src.js"></script>
<script src="/public/js/chart/data.js"></script>
<script src="/public/js/chart/chart.js"></script>
@endpush