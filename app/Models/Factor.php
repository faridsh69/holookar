<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factor extends Model
{
	use SoftDeletes;

    protected $guarded = [];
    protected $hidden = [
    	'deleted_at',
    	'updated_at',
    ];

    protected $fillable = [
		'id',
		'total_price',
		'shipping', 
		'payment', 
		'user_description',
		'admin_description',
		'status',
        'admin_seen',
		'address_id',
        'user_id',
		'admin_id',
	];

    const SHIPPING_MAMOLI = 'معمولی';
    const SHIPPING_FAST = 'سریع';
    const SHIPPING_SEFARESHI = 'سفارشی';

    const PAYMENT_LOCAL = 'پرداخت در محل';
    const PAYMENT_CART = 'پرداخت کارت به کارت';
    const PAYMENT_ONLINE = 'پرداخت آنلاین';

    const STATUS_INITIAL = 1;
    const STATUS_PAYMENT = 2;
    const STATUS_PROCCESSING = 3;
    const STATUS_PREPARING = 4;
    const STATUS_DELIVERING = 5;
    const STATUS_CANCELED = 6;
    const STATUS_SUCCEED = 7;

    public static $STATUS = [
        self::STATUS_INITIAL => 'ثبت اولیه سفارش',
        self::STATUS_PAYMENT => 'انتقال به درگاه بانک',
        self::STATUS_PROCCESSING => 'درحال بررسی سفارش',
        self::STATUS_PREPARING => 'درحال آماده سازی کالاها',
        self::STATUS_DELIVERING => 'تحویل به سیستم حمل و نقل',
        self::STATUS_CANCELED => 'سفارش لغو گردید',
        self::STATUS_SUCCEED => 'سفارش تحویل داده شد',
    ];

    public static function getFillables()
    {
    	return (new self)->fillable;
    }

    public function status_translate()
    {
        if(isset( self::$STATUS[$this->status] ) ){
        	return self::$STATUS[$this->status];

        }else{
            return 'وضعیت ندارد';
        }
    }

    public function scopeMine($query)
    {
        return $query->where('user_id', \Auth::id());
    }

    public function scopeCurrentFactor($query)
    {
        return $query->where('user_id', \Auth::id())
            ->where('status', '<', 3)
            // ->where('admin_seen', 0)
            // ->where('created_at', '>', carbon::now()->subHour() )
            ->orderBy('id', 'desc');            
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function last_payment()
    {
        return $this->payments()->orderBy('id', 'desc')->first();
    }
    
    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('count')->withPivot('price')->withPivot('discount_price');
    }

    public function scopeCheckUserRole($query, $role_id)
    {
        if($role_id)
        {
            $role = \App\Models\Role::find($role_id);
            if($role){
                $role_name = $role->name;
                return $query->whereHas('user.roles', function($user_query) use ($role_name){
                        $user_query->where('name', $role_name);
                    });
            }else{
                return $query;
            }
        }else{
            return $query;
        }
    }

    public function total_price_products()
    {
        $factor_product = $this->products()->get();
        $total_price = 0;
        foreach($factor_product as $item)
        {
            if($item->pivot->discount_price){
                $total_price = $total_price + ( $item->pivot->count * $item->pivot->discount_price );
            }else{
                $total_price = $total_price + ( $item->pivot->count * $item->pivot->price );
            }
        }
        return $total_price;
    }

    public function tagends()
    {
        return $this->belongsToMany('App\Models\Tagend')->withPivot('value');;
    }

    public function calculateTotalPriceWithTagends()
    {
        $total_price = $this->total_price_products();

        foreach( $this->tagends as $tagend)
        {
            $total_price = $total_price + $tagend->pivot->value; 
        }
        return $total_price;
    }



    // public function updateOrder($items, $calculateNewTagEnd = true){
    //     $currentItems = FactorItem::find()->where(['factorId' => $this->id])->indexBy('priceId')->all();
    //     $total = 0;
    //     $this->_bonTrans = [];
    //     $this->_bonUserCanUsed = 0;
    //     $transaction = \Yii::$app->db->beginTransaction();
    //     try{
    //         // first delete bon transaction
    //         if($this->userId !== null){
    //             foreach( BonTransactions::findAll(['factorId' => $this->id]) as $t ){
    //                 $this->_bonTrans[$t['bonId']] = isset($this->_bonTrans[$t['bonId']]) ? $this->_bonTrans[$t['bonId']]+$t->value : $t->value;
    //                 if($t->delete() === false)
    //                     throw new Exception( '(error code #001) \n Please try again!' );
    //             }
    //         }

    //         // add BonTransaction of unchanged priceIds in the order
    //         foreach($currentItems as $priceId => $item){
    //             if(!array_key_exists($priceId, $items)){
    //                 if(($price = $item->price) !== null){
    //                     $this->_bonUserCanUsed +=  $price->bonUsed * $item->num;
    //                     $total += $item->getRealPrice();

    //                     // add Bon Transaction
    //                     $this->addBonTrans();
    //                 }
    //             }
    //         }

    //         foreach($items as $priceId => $p){
    //             $num = $p['num'];
    //             $unit = $p['unit'];
    //             $discount = $p['discount'];

    //             if(($price = Price::findOne($priceId)) === null)
    //                 continue;

    //             $this->_bonUserCanUsed +=  $price->bonUsed * $num;

    //             if(array_key_exists($priceId, $currentItems)){
    //                 // update item
    //                 $currentItems[$priceId]->num = $num;
    //                 $currentItems[$priceId]->unit = $unit;
    //                 $currentItems[$priceId]->discount = $discount;
    //                 $currentItems[$priceId]->save();

    //                 $total += $currentItems[$priceId]->getRealPrice();

    //                 // update init value of this bon
    //                 if($this->userId !== null && ($bon = Bon::findOne(['factorId' => $this->id, 'priceId' => $priceId])) !== null){
    //                     $newBon = $price->bon * $num;
    //                     $bon->remainValue += $newBon - $bon->initValue;
    //                     $bon->initValue = $newBon;
    //                     if(!$bon->save()){
    //                         throw new Exception( '(error code #002) \n Please try again!' );
    //                     }
    //                 }
    //             }else{
    //                 // create new item
    //                 if(($price = Price::findOne($priceId)) === null)
    //                     continue;

    //                 $item = new FactorItem();
    //                 $item->factorId = $this->id;
    //                 $item->priceId = $priceId;
    //                 $item->unit = $unit;
    //                 $item->discount = $discount;
    //                 $item->num = $num;
    //                 if(!$item->save()){
    //                     throw new Exception( '(error code #003) \n Please try again!' );
    //                 }

    //                 $total += $item->getRealPrice();

    //                 // add new Bon
    //                 if($this->userId !== null and $price->bon != null){
    //                     $bon = new Bon();
    //                     $bon->userId = $this->userId;
    //                     $bon->initValue = $price->bon * $num;
    //                     $bon->remainValue = $price->bon * $num;
    //                     $bon->factorId = $this->id;
    //                     $bon->priceId = $price->id;
    //                     if(!$bon->save(false)){
    //                         throw new Exception( '(error code #004) \n Please try again!' );
    //                     }
    //                 }
    //             }
    //             // add Bon Transaction
    //             $this->addBonTrans();
    //         } // end foreach of items


    //         $tags = [];
    //         if($calculateNewTagEnd){
    //             // delete all factor tag end transaction of the order
    //             FactorTagEndTransactions::deleteAll(['factorId' => $this->id]);
    //             //save tagEnd in tagEndTransactions, return ['tags' => $tags, 'total' => $totalOfTagsEnds],
    //             $tags = FactorTagEnd::register($this->userId, $this, $total);
    //             FactorTagEndTransactions::saveAll($tags['tags'], $this->id);
    //         }

    //         // calculate total order
    //         $this->total = $this->calculateSumOrder($total, $tags);

    //         if(!$this->save()){
    //             throw new Exception( '(error code #005) \n Please try again!' );
    //         }

    //         $transaction->commit();
    //     }catch (Exception $e){
    //         $transaction->rollBack();
    //         throw $e;
    //     }
    // }


    //  public function getTotalItems(){
    //     $totalItems = 0;
    //     foreach($this->items as $item){
    //         $totalItems += $item->getRealPrice();
    //     }
    //     return $totalItems;
    // }

    // /**
    //  * calculate total
    //  * @return bool successful save new total
    //  */
    // public function resetTotal(){
    //     $this->total = $this->calculateSumOrder($this->getTotalItems());
    //     return $this->save();
    // }

    // /**
    //  * @param int $total total of factorItems in the order
    //  * @param FactorTagEnd[] $tags, if array empty given, the already of factor tag ends will calculate
    //  * @return int
    //  */
    // private function calculateSumOrder($total, $tags = []){
    //     // calculate sum of tag end
    //     if(count($tags) === 0){ // if tags not given, we calculate from existence of tag end of order
    //         $sumTagEnd = FactorTagEnd::calculateSumExistsTagEnds($this->id);
    //     }else{
    //         $sumTagEnd = $tags['total']; // sum price of tag end
    //     }
    //     $sumBon = BonTransactions::find()->select('sum([[value]]) as sum')->where(['factorId' => $this->id])->asArray()->one();
    //     $sumBon = $sumBon['sum'] === null ? 0 : (int)$sumBon['sum'] * 1000;


    //     $totalOrder = $total - $sumBon + $sumTagEnd;
    //     $totalOrder = $totalOrder < 0 ? 0 : $totalOrder;
    //     return $totalOrder;
    // }


    // /**
    //  * @throws Exception
    //  */
    // private function addBonTrans(){
    //     // first used bon from current bon that we used early

    //     if($this->userId !== null && count($this->_bonTrans) && $this->_bonUserCanUsed){
    //         // sort bonTrans desc
    //         arsort($this->_bonTrans);
    //         foreach($this->_bonTrans as $bonId => $val){
    //             if( $this->_bonUserCanUsed === 0 ){
    //                 break;
    //             }
    //             if(($oldBonTrans = Bon::findOne($bonId))!== null){
    //                 if($val >= $this->_bonUserCanUsed){
    //                     $transValue = $this->_bonUserCanUsed;
    //                     $oldBonTrans->remainValue -=  $this->_bonUserCanUsed;
    //                     $this->_bonTrans[$bonId] -= $this->_bonUserCanUsed;
    //                     $this->_bonUserCanUsed = 0;
    //                 }else{
    //                     $transValue = $val;
    //                     // $val < $bonUserCanUsed, so $bonUserCanUsed after "-" it is a positive number
    //                     $this->_bonUserCanUsed -= $val;
    //                     $oldBonTrans->remainValue -= $val;
    //                     unset($this->_bonTrans[$bonId]);
    //                 }
    //                 if(!$oldBonTrans->save()){
    //                     throw new Exception( '(error code #006) \n Please try again!' );
    //                 }

    //                 $bonTransactions = new BonTransactions;
    //                 $bonTransactions->bonId = $bonId;
    //                 $bonTransactions->factorId = $this->id;
    //                 $bonTransactions->value = $transValue;
    //                 if(!$bonTransactions->save(false)){
    //                     throw new Exception( '(error code #007) \n Please try again!' );
    //                 }
    //             }
    //         }
    //     }
    //     // now if already $bonUserCanUsed has remained, check for other available active bon of the user
    //     if($this->userId !== null && $this->_bonUserCanUsed){
    //         $bon = Bon::find()
    //             ->where(['userId' => $this->userId, 'status' => true])
    //             ->andWhere('expiredTime > NOW()')
    //             ->andWhere('remainValue > 0')
    //             ->orderBy('remainValue desc')
    //             ->all();
    //         foreach($bon as $val){
    //             if( $this->_bonUserCanUsed === 0 ){
    //                 break;
    //             }

    //             if($val->remainValue >= $this->_bonUserCanUsed){
    //                 $value = $this->_bonUserCanUsed;
    //                 $val->remainValue -=  $this->_bonUserCanUsed;
    //                 $this->_bonUserCanUsed = 0;
    //             }else{
    //                 $value = $val->remainValue;
    //                 // $val->remainValue < $bonUserCanUsed, so $bonUserCanUsed after "-" it is a positive number
    //                 $this->_bonUserCanUsed -= $val->remainValue;
    //                 $val->remainValue = 0;
    //             }
    //             if(!$val->save(false)){
    //                 throw new Exception( '(error code #008) \n Please try again!' );
    //             }

    //             $bonTransactions = new BonTransactions;
    //             $bonTransactions->bonId = $val->id;
    //             $bonTransactions->factorId = $this->id;
    //             $bonTransactions->value = $value;
    //             if(!$bonTransactions->save(false)){
    //                 throw new Exception( '(error code #009) \n Please try again!' );
    //             }
    //         }

    //     }
    // }


}
