<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContentArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'title' => 'required|max:255',
	        'top_title' => 'max:255',
	        'sub_title' => 'max:255',
	        'content' => 'required',
	        'meta_title' => 'max:70',
	        'meta_description' => 'max:200',
	        'category_id' => 'nullable|exists:categories,id',
	    ];
    }
}
