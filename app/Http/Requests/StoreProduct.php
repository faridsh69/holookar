<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'title' => 'required|max:255',
	        // 'price' => 'required|numeric',
	        // 'inventory' => 'required|numeric',
	        // 'discount_price' => 'nullable|numeric',
	        'meta_title' => 'max:255',
	        'meta_description' => 'max:255',
	        'keywords' => 'max:255',
	        // 'brand_id' => 'nullable|exists:brands,id',
	        // 'category_id' => 'nullable|exists:categories,id',
	    ];
    }
}
