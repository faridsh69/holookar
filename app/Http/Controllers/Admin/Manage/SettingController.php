<?php

namespace App\Http\Controllers\Admin\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Setting;
use \App\Http\Requests\StoreSetting;

class settingController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:general_manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	// self::Setting();
	   	$settings = Setting::orderBy('id', 'asc')->get();   
	
		return view('admin.manage.setting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manage.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSetting $request)
    {
        dd('این بخش را به ۰۹۱۰۶۸۰۱۶۸۵ گزارش دهید و عدد ۱۲۰ را گزارش دهید');
    	// creating new object for save request
    	$setting = new Setting();
    	foreach (Setting::getFillables() as $key) {
    		if( isset($request[$key]) ){
	    		$setting[$key] = $request[$key];
    		}
    	}
    	$setting->save();
    	\Cache::forget('site-constant');
  //   	$cache_settings = \Cache::remember('site-constant', 6000, function () {
		//     return \App\Models\Setting::pluck('value','key');
		// });
       
    	if($setting){
	    	$request->session()->flash('alert-success', self::MESSAGE_INSERT_SUCCESS);
    	}else{
	    	\Log::error('در هنگام ثبت خطا رخ داده است - user with user_id : '. \Auth::id() );
	    	$request->session()->flash('alert-danger', self::MESSAGE_FAILED);
    	}
        return redirect('/admin/manage/setting');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = Setting::where('id',$id)->first();

        if($setting){
	        return view('admin.manage.setting.show', compact('setting') );
        }else{
        	return redirect('/admin/manage/setting');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::where('id',$id)->first();

        if($setting){
	        return view('admin.manage.setting.create', compact('setting') );
        }else{
        	return redirect('/admin/manage/setting');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSetting $request, $id)
    {
    	$setting = Setting::where('id',$id)->first();
        if(!$setting){
        	return redirect('/admin/manage/setting');
        }
        if($setting->key == 'logo' || $setting->key == 'default_image' || $setting->key == 'default_image_user' || $setting->key == 'default_image_product' || $setting->key == 'favicon'){
            $url = \App\Http\Controllers\ImageController::saveSetting($request['value'], $setting->key);
            $setting['value'] = '/' . $url;
        }else{
            $setting['value'] = $request->input('value');
	    }
        $setting->save();

    	if($setting){
	    	$request->session()->flash('alert-success', self::MESSAGE_UPDATE_SUCCESS);
    	}else{
	    	\Log::error('در هنگام ثبت خطا رخ داده است - user with user_id : '. \Auth::id() );
	    	$request->session()->flash('alert-danger', self::MESSAGE_FAILED);
    	}
        return redirect('/admin/manage/setting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $setting = Setting::where('id',$id)->first();
        if($setting)
        {
        	$setting->delete();
        	$request->session()->flash('alert-success', self::MESSAGE_DELETE_SUCCESS);

        	return redirect('/admin/manage/setting');
        }
        else{
        	$request->session()->flash('alert-danger', self::MESSAGE_NOT_FOUND);
        	\Log::error('در هنگام حذف خطا رخ داده است - user with user_id : '. \Auth::id() );

        	return redirect('/admin/manage/setting');
        }
    }
}
