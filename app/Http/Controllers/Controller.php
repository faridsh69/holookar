<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Product;
use App\Models\Category;
use App\Models\Basket;
use App\Models\Factor;
use App\Models\Feature;
use App\Models\ProductView;
use App\Models\User;

class Controller extends BaseController
{  
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const PAGE_SIZE = 20;
    const MESSAGE_INSERT_SUCCESS = 'آیتم مورد نظر با موفقیت ذخیره شد.';
    const MESSAGE_UPDATE_SUCCESS = 'تغییرات شما با موفقیت ذخیره شد.';
    const MESSAGE_FAILED = 'در هنگام ثبت خطا رخ داده است.';
    const MESSAGE_DELETE_SUCCESS = 'آیتم مورد نظر با موفقیت حذف شد.';
    const MESSAGE_NOT_FOUND = 'آیتم مورد نظر یافت نشد';
    
    public static function setting()
    {
        if( class_exists('Config') ){
            if( \Schema::hasTable('settings') ){
                return \App\Models\Setting::pluck('value','key');
            }
        }
        $settings = self::DEFAULT_SETTINGS;
        $default_setting = collect($settings)->pluck('value', 'key');
        return $default_setting;
        

      //   try
      //   {   
      //  		$cache_settings = \Cache::remember('site-constant', 6000, function () {
    		//     return \App\Models\Setting::pluck('value','key');
    		// });
    		// return $cache_settings;
      //   }
      //   catch(Exception $e) {
      //       return \App\Models\Setting::pluck('value','key');
      //   }
    }

    public static function _getProductsVue($filters = false)
    {
        $products = Product::Mojod()
            ->select('id','title','price','discount_price')
            ->orderBy('id', 'desc')
            ->with('images')
            ->get();

        if($filters)
        {
            if($filters['category'])
            {
                $category = Category::where('title', $filters['category'])->first();
                if($category)
                {
                    $category_id = $category->id;
                    $products = $products->where('category_id', $category_id);
                }
            }
            // if($filters['title'])
            // { 
            //     $products = $products->where('title', 'like','%'. $filters['title'] .'%');
            // }
        }
        $vue_products = [];
        foreach($products as $product)
        {
            $vue_products[] = [
                'id' => $product->id,
                'title' => $product->title,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'image_url' => $product->base_image(),
            ];
        }
        return $vue_products;
    }

    public static function _getUserBasketVueProducts()
    {
        $basket = self::_getUserBasket();
        $vue_basket = [];
        foreach($basket->products as $basket_product)
        {
            $vue_basket[] = [
                'id' => $basket_product->id,
                'title' => $basket_product->title,
                'price' =>  $basket_product->price,
                'discount_price' =>  $basket_product->discount_price,
                'count' =>  $basket_product->pivot->count,
                'image_url' => $basket_product->base_image(),
            ];
        }
        return $vue_basket;
    }

    public static function _getUserBasketCountProducts()
    {
        $basket = self::_getUserBasket();

        // $basket = Basket::where('user_id', \Auth::id())
        //         ->orderBy('id', 'desc')
        //         ->first();

        $count = 0;
        foreach($basket->products as $basket_product)
        {
            $count ++;
        }
        return $count;
    }

    public static function _getUserBasket()
    {
        $basket_id = session('basket_id', null);
        session(['basket_id' => null]);
        if($basket_id)
        {
            $basket = Basket::where('id', $basket_id)
                ->orderBy('id', 'desc')
                ->first();
            if($basket)
                return $basket;
        }

        if(\Auth::id())
        {
            if(isset($_COOKIE['basket_id']))
            {
                $basket_id = $_COOKIE['basket_id'];
                $basket = Basket::where('id', $basket_id)
                    ->whereNull('user_id')
                    ->orderBy('id', 'desc')
                    ->first();
                if($basket){
                    $basket->user_id = \Auth::id();
                    $basket->save();
                    setcookie("basket_id", "", time() - 3600);

                    return $basket;
                }
            }
            $basket = Basket::where('user_id', \Auth::id())
                ->orderBy('id', 'desc')
                ->first();
            if($basket){
                return $basket;
            }

        }
        else
        {
            if(isset($_COOKIE['basket_id']))
            {
                $basket_id = $_COOKIE['basket_id'];
                $basket = Basket::where('id', $basket_id)
                    ->whereNull('user_id')
                    ->orderBy('id', 'desc')
                    ->first();
                if($basket){
                    return $basket;
                }
            }
        }

        $basket = self::_create_basket();
        if($basket)
            return $basket;

        return 'مشکلی در سیستم وجود دارد';
    }

    public static function _create_basket()
    {
        $basket = new Basket();
        $basket->status = Basket::STATUS_ACTIVE;
        $basket->user_id = \Auth::id();
        $basket->save();

        setcookie('basket_id', $basket->id, time() + (86400 * 3), "/"); // 86400 = 1 day
        session(['basket_id' => $basket->id]);

        return $basket;
    }


    public static function _getTotalPrice()
    {
        $basket = self::_getUserBasket();
        $basket_product = $basket->products()->get();
        $total_price = 0;
        foreach($basket_product as $item)
        {
            if($item->discount_price){
                $total_price = $total_price + ( $item->pivot->count * $item->discount_price );
            }else{
                $total_price = $total_price + ( $item->pivot->count * $item->price );
            }
        }
        return $total_price;
    }

    public static function _addToBasket($product_id, $add)
    {
        $basket = self::_getUserBasket();
        $basket_product = $basket->products()->where('product_id', $product_id);
            // ->where('inventory', '>', 0);
        if($basket_product->count() >= 1){
            $count = $basket_product->first()->pivot->count + $add;
            if($count == 0 && $add == -1){
                $basket->products()->detach([$product_id]);
            }else{
                $basket->products()->syncWithoutDetaching([$product_id => ['count' => $count ]], false);
            }
        }
        else{
            $basket->products()->syncWithoutDetaching([$product_id => ['count' => 1 ]]);
        }
    }

    public static function _changeCountBasket($product_id, $count)
    {
        $basket = self::_getUserBasket();
        if($count == 0){
            $basket->products()->detach([$product_id]);
        }else{
            $basket->products()->syncWithoutDetaching([$product_id => ['count' => $count ]]);
        }
    }

    public static function _getErrorZarrinpal($status)
    {
        switch ($status) {
        case '-1':
            return 'اطلاعات ارسال شده ناقص است.';
            break;
        case '-2':
            return 'آی پی یا مرچنت کد پذیرنده صحیح نیست';
            break;
        case '-3':
            return 'با توجه به محدودیت های شاپرک امکان پرداخت با رقم درخواست شده میسر نمی باشد.';
            break;
        case '-4':
            return 'سطح تایید پذیرنده پایین تر از صطح نقره ای است.';
            break;
        case '-11':
            return 'درخواست مورد نظر یافت نشد.';
            break;
        case '-12':
            return 'امکان ویرایش درخواست میسر نمی باشد.';
            break;
        case '-21':
            return 'هیچ نوع عملیات مالی برای این تراکنش یافت نشد.';
            break;
        case '-22':
            return 'تراکنش نا موفق می باشد.';
            break;
        case '-33':
            return 'رقم تراکنش با رقم پرداخت شده مطابقت ندارد.';
            break;
        case '-34':
            return 'سقف تقسیم تراکنش از لحاظ تعداد با رقم عبور نموده است.';
            break;
        case '-40':
            return 'اجازه دسترسی به متد مربوطه وجود ندارد.';
            break;
        case '-41':
            return 'اطلاعات ارسال شده مربوط به AdditionalData غیر معتر می باشد.';
            break;
        case '-42':
            return 'مدت زمان معتبر طول عمر شناسه پرداخت بین ۳۰ دقیقه تا ۴۰ روز می باشد.';
            break;
        case '-54':
            return 'درخواست مورد نظر آرشیو شده است.';
            break;
        case '100':
            return 'عملیات با موفقیت انجام گردیده است.';
            break;
        case '101':
            return 'عملیات پرداخت موفق بوده و قبلا Payment Verification تراکنش انجام شده است';
            break;
        default:
            return 1000;
            break;
        };
    }

    const DEFAULT_SETTINGS = [
        [
            'key' => 'name',
            'value' => 'وهاب باطری',
            'description' => 'نام سامانه',
        ],
        [
            'key' => 'description',
            'value' => 'وهاب باطری در زمینه تامین باتری در ایران با سابقه ترین شرکت داخلی می‌باشد.',
            'description' => 'توضیحات',
        ],
        [
            'key' => 'phone',
            'value' => '02166124999',
            'description' => 'تلفن دفتر',
        ],
        [
            'key' => 'mobile',
            'value' => '09106801685',
            'description' => 'شماره همراه',
        ],
        [
            'key' => 'email',
            'value' => 'farid.sh69xx@gmail.com',
            'description' => 'ایمیل',
        ],
        [
            'key' => 'fax',
            'value' => '021661246',
            'description' => 'فکس',
        ],
        [
            'key' => 'address',
            'value' => 'تهران- خییابان کاشانی - کوچه بهنام',
            'description' => 'آدرس دفتر',
        ],
        [
            'key' => 'telegram',
            'value' => 'vahabbatri',
            'description' => 'آدرس تلگرام',
        ],
        [
            'key' => 'instagram',
            'value' => 'vahabbatri',
            'description' => 'آدرس اینستاگرام',
        ],
        [
            'key' => 'card_number',
            'value' => '6037-6916-6097-xxxx',
            'description' => 'شماره کارت فروشگاه',
        ],
        [
            'key' => 'account_number',
            'value' => '1302502550221',
            'description' => 'شماره حساب فروشگاه',
        ],
        [
            'key' => 'merchant_code',
            'value' => '213124123',
            'description' => 'کد مشتری درگاه بانک',
        ],
        [
            'key' => 'terminal_code',
            'value' => '213124123',
            'description' => 'کد ترمینال درگاه بانک',
        ],
        [
            'key' => 'zarin_pal',
            'value' => 'ea12b276-429a-11e7-a249-005056a205be',
            'description' => 'درگاه زرین بال',
        ],
        [
            'key' => 'email_sender',
            'value' => 'farid.sh69@gmail.com',
            'description' => 'ایمیل مختص ارسالی به کاربران',
        ],
        [
            'key' => 'email_sender_password',
            'value' => 'ccixkuedlxpbizda',
            'description' => 'رمز ایمیل مختص ارسالی به کاربران',
        ],
        [
            'key' => 'sms_user',
            'value' => 's.farid.sh69',
            'description' => 'نام کاربری پنل اس ام اس',
        ],
        [
            'key' => 'sms_pass',
            'value' => '39026',
            'description' => 'رمز عبور پنل اس ام اس',
        ],
        [
            'key' => 'sms_phone',
            'value' => '50005708616261',
            'description' => 'شماره پنل اس ام اس',
        ],
        [
            'key' => 'sms_url',
            'value' => 'http://payamak-service.ir/SendService.svc?wsdl',
            'description' => 'url server sms',
        ],
        [
            'key' => 'logo',
            'value' => '/upload/images/logo.png',
            'description' => 'لوگو',
        ],
        [
            'key' => 'default_image',
            'value' => '/upload/images/default.png',
            'description' => 'عکس پیش فرض در سیستم',
        ],
        [
            'key' => 'default_image_user',
            'value' => '/upload/images/default_user.png',
            'description' => 'عکس پیش فرض در سیستم',
        ],
        [
            'key' => 'default_image_product',
            'value' => '/upload/images/default_product.png',
            'description' => 'عکس پیش فرض در سیستم',
        ],
        [
            'key' => 'favicon',
            'value' => '/upload/images/favicon.png',
            'description' => 'لوگو روی تب',
        ],
        [
            'key' => 'enamad',
            'value' => '123124123123123',
            'description' => 'کد اعتماد الکترونیکی',
        ],
        [
            'key' => 'google_analytics',
            'value' => 'UA-97891904-1',
            'description' => 'کد گوگل آنالیتیکس',
        ],
        [
            'key' => 'crisp',
            'value' => 'UA-97891904-1',
            'description' => 'چت آنلاین',
        ],
        [
            'key' => 'latitude',
            'value' => '35.2',
            'description' => 'عرض جغرافیایی',
        ],
        [
            'key' => 'longitude',
            'value' => '53.5',
            'description' => 'طول جغرافیایی',
        ],
        [
            'key' => 'register_sms',
            'value' => 'با سلام. \n به فروشگاه خود خوش آمدید',
            'description' => 'اس ام اس ارسالی در موقع ثبت نام',
        ],
        [
            'key' => 'register_email',
            'value' => 'با سلام. \n به فروشگاه خود خوش آمدید',
            'description' => 'ایمیل ارسالی در موقع ثبت نام',
        ],
        [
            'key' => 'shopping_sms',
            'value' => 'سفارش شما در سیستم ثبت شد. مسولین در حال آماده سازی کالاها می باشند.',
            'description' => 'اس ام اس ارسالی در موقع ثبت سفارش',
        ],
        [
            'key' => 'shopping_email',
            'value' => 'سفارش شما در سیستم ثبت شد. مسولین در حال آماده سازی کالاها می باشند.',
            'description' => 'ایمیل ارسالی در موقع ثبت سفارش',
        ],
        [
            'key' => 'advertise_why_not_accept_status',
            'value' => "این آگهی شامل اطلاعات نادرست است.+ در این آگهی اطلاعات ناکافی است.+ این آگهی مورد تایید نیست.",
            'description' => 'دلایل رد شدن آگهی ها',
        ],
        [
            'key' => 'theme',
            'value' => 'batri',
            'description' => 'نام سامانه',
        ],
    ];
}
