<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Factor;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;
use Carbon\Carbon;


class FactorController extends Controller
{
    public function index()
    {       

    	$data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $factors = Factor::paginate(6);
        foreach($factors as $factor)
        {
            $data[] = [
                'id' => $factor->id,
                'total_price' => $factor->total_price,
                'status' => $factor->status,
                'payment' => $factor->payment,
                'shipping' => $factor->shipping,
                'address' => $factor->address(),
                'user_id' => $factor->user(),
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function show($id)
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $factor = Factor::find($id);
        $products = $factor->products;
        if($factor)
        {
            $data = [
                'id' => $factor->id,
                'total_price' => $factor->total_price,
                'products' => $products,
                'status' => $factor->status,
                'payment' => $factor->payment,
                'shipping' => $factor->shipping,
                'address' => $factor->address(),
                'user_id' => $factor->user(),
            ];
            //die($data);
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }

        $data['message'] = $message;
        $data['status'] = $status;
        return $data;
    }


    public function getuserfactores(Request $request){

        $user_factors = Factor::where('user_id',$request['user_id'])->get();

        foreach($user_factors as &$factor){

            $factor->address = $factor->address;
            $factor->status = $factor->status_translate();
           // $factor->products = $factor->products;

        }
        $data['factors'] = $user_factors;
        $data['message'] = "Done";
        $data['status'] = 1;
        return $data;



    }

    public function getuserfactordetails(Request $request){

        $user_factors = Factor::where('user_id',$request['user_id'])->where('id',$request['factor_id'])->get();

        foreach($user_factors as &$factor){

           //$factor->address = $factor->address;
            $factor->user_name = $factor->user->name();
            $factor->user_name = $factor->user->name();
            $date = Carbon::parse($factor->created_at);
            $factor->date = $date->timestamp;
            $factor->status = $factor->status_translate();
            $factor->products = $factor->products;

        }
        $data['factors'] = $user_factors;
        $data['message'] = "Done";
        $data['status'] = 1;
        return $data;



    }

}
