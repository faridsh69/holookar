<?php

namespace App\Http\Controllers\Api;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Advertise;

use \App\Models\Article;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;

class CategoryController extends Controller
{
    public function index()
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $categories = Category::get();
        foreach($categories as $category)
        {
            $data[] = [
                'id' => $category->id,
                'type' => $category->type,
                'title' => $category->title,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }


    public function showcategoryproducts(Request $request){

        $products = Product::where('category_id', $request['category_id'])->get();

        foreach($products as $key => $product ) {

            $out[$key]['title'] = $product['title'];
            $out[$key]['id'] = $product['id'];
            $out[$key]['brand'] = $product->brand['title'];
            $out[$key]['category'] = $product->category['title'];
            $out[$key]['price'] = $product['price'];

            $date = Carbon::parse($product->created_at);
            $out[$key]['date'] = $date->timestamp;

            $out[$key]['photo'] = url('/') . $product->base_image();

        }

        $data['status'] = 1;
        $data['message'] = "Done";
        $data['products'] = $out;
        return $data;

    }

    public function showcategoryadvertise(Request $request){
        $out = [];
        $advertises = Advertise::where('category_id', $request['advertise_id'])->get();

        foreach($advertises as $key => $advertise ) {

            $out[$key]['title'] = $advertise['title'];
            $out[$key]['id'] = $advertise['id'];
            $out[$key]['brand'] = $advertise->brand['title'];
            $out[$key]['category'] = $advertise->category['title'];
            $out[$key]['price'] = $advertise['price'];

            $date = Carbon::parse($advertise->created_at);
            $out[$key]['date'] = $date->timestamp;

            $out[$key]['photo'] = url('/') . $advertise->gallery_first_image();
            $out[$key]['province'] = $advertise['province'];
            $out[$key]['city'] = $advertise['city'];

        }

        $data['status'] = 1;
        $data['message'] = "Done";
        $data['products'] = $out;
        return $data;

    }









}
