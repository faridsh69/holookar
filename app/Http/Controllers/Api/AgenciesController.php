<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use Illuminate\Support\Facades\DB;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\User;
use \App\Models\Article;
use \App\Models\Advertise;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;



class RepresentationController extends Controller
{

    public function index(){


    $representations = User::getRepresentations()->get();
//    $representations = $representations->get();
    foreach($representations as $representation){
        $representation->addresses;
        $representation->photo = url('/').$representation['avatar']['src100'];
    }

        $data['status'] = 1;
        $data['message'] = 'done';
        $data['representations'] = $representations;

        return $data;
}

    public function filter(Request $request){


//        $representations = User::getRepresentations();
//        $representations = $representations->get();
//        foreach($representations as $representation){
//            $representation = $representation->addresses;
//        }

        $where = "";

        if($request->has('city')){
            $where .= 'addresses.city like '. '"%' .$request['city'] . '%"' . ' and ';
        }

        if($request->has('province')){
            $where .= 'addresses.province = '.$request['province'] . ' and ';
        }



        $where = substr($where,0,-4);


        $query = "
                select 
                
                addresses.user_id as user_id,
                addresses.display_name as user_name,
                addresses.city,
                addresses.province,
                addresses.address,
                addresses.phone as mobile_phone,
                addresses.sabet_phone as phone,
                role_user.role_id,
                users.image_id as photo_id,
                concat('".url('/')."',images.src100) as photo
                
                from addresses
                
                left join role_user on addresses.user_id = role_user.user_id
                left join users on addresses.user_id = users.id
                left join images on users.image_id = images.id
                
                
                
                where $where  AND role_user.role_id = 1
        ";


        //echo $query;


        $result = DB::select($query);

        foreach ($result as &$item){
            $item->address = array(
                "province" => $item->address,
                "city" => $item->city,
                "address" => $item->address,
                "phone" => $item->phone,
                "sabet_phone" => $item->mobile_phone,

            );
        }




        $data['status'] = 1;
        $data['message'] = 'done';
        $data['representations'] = $result ? $result : NULL;

        return $data;
    }

    public function getByBrands(Request $request){
        $brand_id = $request['brand_id'];
        $users = User::whereHas('brands', function($query) use ($brand_id){
            return $query->where('brand_id', $brand_id);
        })->get();

        foreach ($users as $user){
            $user->photo = $user->avatar->src100 ? url('/').$user->avatar->src100 : null;
        }

        $data['status'] = 1;
        $data['message']= "done";
        $data['representations'] = $users;
        return $data;

    }

}

