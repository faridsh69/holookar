<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;

class BrandController extends Controller
{
    public static function index()
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $brands = Brand::get();
        foreach($brands as $brand)
        {
            $data[] = [
                'id' => $brand->id,
                'image_url' => $brand->image ? URL('/').$brand->image->src : null,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function GetBrandsWithImages(){
        $brands = Brand::get();
        foreach($brands as $brand)
        {
            $data[] = [
                'id' => $brand->id,
                'title' => $brand->title,
                'image_url' => $brand->image ? URL('/').$brand->image->src : null,
            ];
        }

        return $data;

    }
}
