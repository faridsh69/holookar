<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Http\Requests\StoreProduct;

class ArticleController extends Controller
{
    public function index()
    {
    	$data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $articles = Article::get();
        foreach($articles as $article)
        {
            $data[] = [
                'id' => $article->id,
                'title' => $article->title,
                'description' => $article->description,
                'image_url' => $article->image ? URL('/').$article->image->src : null,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }
}
