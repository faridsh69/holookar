<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Brand;
use \App\Models\Baner;
use \App\Http\Requests\StoreProduct;

class BannerController extends Controller
{
    public static function get_main_banners()
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $banners = Baner::where('location',1)->get();


        foreach($banners as $banner)
        {
            $data[] = [
                'id' => $banner->id,
                'image_url' => $banner->image ? URL('/').$banner->image->src : null,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public static function get_ads_banners()
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $banners = Baner::where('location',2)->get();;
        foreach($banners as $banner)
        {
            $data[] = [
                'id' => $banner->id,
                'image_url' => $banner->image ? URL('/').$banner->image->src : null,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

}
