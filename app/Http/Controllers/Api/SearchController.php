<?php
namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Brand;
use \App\Models\Advertise;
use \App\Http\Requests\StoreProduct;


class SearchController extends Controller
{

    public function index(Request $request){

        $keyword = $request['keyword'];

        $data['products'] = Product::where('title','like','%'.$keyword.'%')->get();
        foreach ($data['products'] as &$product){
            $product->photo = url('/').$product->base_image();
        }
        $data['advertises'] = Advertise::where('title','like','%'.$keyword.'%')->get();
        foreach ($data['advertises'] as &$advertise){
            foreach($advertise->images as $image) {
                $advertise->photo = url('/') . $image->src;
                $advertise->images = NULL;
                break;
            }

        }


        $data['status'] = 1;
        $data['message'] = "done";
        return $data;

    }

    public function searchByFilter(Request $request){

        $where = "";
        if($request['type'] == 'products'){
            $title = 'products';

        }else{
            $title = 'advertises';
        }
        if($request->has('keyword')){

            $where .= "$title.title like ". '"%' .$request['keyword'] . '%"' . ' and ';
        }

        if($request->has('category_id') and ($request['category_id'] !='')){
            $where .= "$title.category_id = ".$request['category_id'] . ' and ';
        }

        if($request->has('brand_id') and ($request['brand_id'] !='')){
            $where .= "$title.brand_id = ".$request['brand_id'] . ' and ';
        }

        if($request->has('city') and ($request['city'] !='')){

            $where .= "$title.city like ". '"%' .$request['city'] . '%"' . ' and ';
        }

        if($request->has('province') and ($request['province'] !='')){
            $where .= "$title.province = ".$request['province'] . ' and ';
        }


        $where = substr($where,0,-4);

//        brands.title as brand_title,
//                    brands.id as brand_id,
        //left join brands on products.brand_id  = brands.id


        $query1 = "select
                    products.title,
                    products.price,
                    products.id,
                    'tehran' as 'city',
                    8 as 'province',
                    products.brand_id,
                    products.category_id,
                    categories.title as category,
                    brands.title as brand,
                    image_product.image_id as image_id,

                    CONCAT('".url('/')."',  images.src100) as photo
                    
                    from products
                    left join brands on products.brand_id  = brands.id
                    left join categories on products.category_id  = categories.id
                    left join image_product on products.id = image_product.id
                    left join images on image_product.image_id = images.id
                    
                     where $where
";

        $query2 = "select
                    advertises.title,
                    advertises.price,
                    advertises.id,
                    advertises.brand_id,
                    UNIX_TIMESTAMP(advertises.created_at) as date,
                    advertises.category_id,
                    advertises.city,
                    advertises.province,
                    categories.title as category,
                    brands.title as brand,
                    advertise_image.id as image_id,
                    
                    
                    CONCAT('".url('/')."',  images.src100) as photo
                    from advertises
                    left join advertise_image on advertises.id = advertise_image.advertise_id
                    left join images on advertise_image.image_id = images.id
                    left join brands on advertises.brand_id  = brands.id
                    left join categories on advertises.category_id  = categories.id


                    
                     where $where
";

        if($request['type'] == 'products'){
            $data['products'] = DB::select($query1);

        }
        else{
            $advertsies = DB::select($query2);
            $ids=array();
            foreach ($advertsies as $product){
                if(in_array($product->id,$ids)){
                    //echo "EXISTS";
                }
                else{
                   // echo $product->id;
                    //echo "ADDED";
                    $data['products'][] = $product;
                }
                array_push($ids,$product->id) ;

                //print_r($ids);

            }

        }
        //echo $where;
        $data['status'] = 1;
        $data['message'] = "done";
        return $data;


    }


}