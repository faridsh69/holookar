<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\User;
use \App\Models\Category;
use \App\Models\Brand;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Setting;
use \App\Models\role;
use \App\Models\Address;
use \App\Models\Like;
use \App\Http\Requests\StoreProduct;
use \App\Http\Requests\UpdateProfile;



class UserController extends Controller
{

    public function postSignUp(Request $request){


        if (User::where('phone', '=', $request['phone'])->exists()) {
            $data['status'] = 0;
            $data['message'] = "شماره تلفن وارد شده، قبلا در سیستم ثبت شده است.";
            return $data;
        }


        $user = \App\Models\User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'status' => 0,
            'email' => $request['email'],
            'sms_code' => rand(1111,9999),
            'password' => bcrypt($request['password']),
        ]);
         //$user->notify(new \App\Notifications\VerificationCode($user->id));
        \Log::info('user register with phone: '. $request['phone']);
        if (\Auth::attempt(['phone' => $request['phone'], 'password' => strtolower($request['password']) ],
            10000 )) {
            session_start();
            $_SESSION['loggedin'] = "true";


           $content = 'ثبت نام شما با موفقیت انجام شد. شما با شماره همراه '.$request['phone'] .' و رمز عبور '. $request['password'] .' در سیستم ثبت نام اولیه انجام داده اید.';
            \App\Http\Controllers\SmsController::send_sms($user, $content);



            $data['status'] = 1;
            $data['user_id'] = $user->id;
            $data['message'] = "Done";
            return $data;
        }
        $data['status'] = 0;
        $data['message'] = "Error. Please check your email or password";


        return $data;
    }

    public function sendactivationcode(Request $request){

        $user = User::where('phone', $request['user_id'])->first();
        $content = 'کد فعال سازی حساب کاربری شما: '.$user['sms_code'];
        \App\Http\Controllers\SmsController::send_sms($user, $content);

        $data['status'] = 1;
        $data['code'] = $user->sms_code;

        return $data;
    }

    public function getactivationcode(Request $request){

        $user = User::where('phone', $request['user_id'])->first();


        if( $user->sms_code == $request['code']){
            $args = [
                'status' => 1
            ];
            User::where('id', $user->id)->update($args);
            $data['status'] = 1;
            $data['message'] = "user has been activated";
        }
        else{
            $data['status'] = 0;
            $data['message'] = "user hasn't been activated";
        }

        return $data;
    }

    public function postLogin(Request $request)
    {
        $message = 'شماره تلفن یا رمز عبور اشتباه است.';
        $status = 0;
        $About = null;
        $profile = null;
        if (\Auth::attempt(['status' => User::STATUS_ACTIVE, 'phone' => $request['phone'], 'password' => $request['password'] ],1440)) {

            session_start();
            $_SESSION['loggedin'] = "true";
            //echo "SESSEION SET";
        }

            $user = User::where('phone', $request['phone'])->first();
        $roles = null;
        if($user)
        {
            if(\Hash::check($request['password'], $user->password))
            {
                foreach(\Auth::user()->roles()->pluck('name') as $role){
                    $roles.=$role.";";
                }
                $profile = [
                    'id' => $user->id,
                    'role' => $roles,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'status' => $user->status,
                    'address' => Address::where('user_id',$user->id)->get(),
                    'img_visitCard' => url('/').$user->visit_image(),
                    'img_businessPlace' => url('/').$user->javaz_image(),
                    'img_store' => url('/').$user->shop_image(),
                    'img_userImage' => $user->avatar->src100 ? url('/'). $user->avatar->src100 : NULL,
                    'id_visitCard' => NULL,
                    'id_businessPlace' => NULL,
                    'id_store' => NULL,
                    'id_userImage' => NULL,
                    'score' => NULL,
                    'view' => NULL,
                    'possession' => NULL,
                    'password'=> NULL,



                ];


                // This is for sending website information to show in app
                $settings = Setting::get();
                $About = [
                    'phone' => $settings->where('key', 'phone')->first()->value,
                    'email' => $settings->where('key', 'email')->first()->value,
                    'address' => $settings->where('key', 'address')->first()->value,
                     'lat' => $settings->where('key', 'phone')->first()->value,
                     'lng' => $settings->where('key', 'phone')->first()->value,
                    'description' => $settings->where('key', 'description')->first()->value,
                ];
                $message = 'کاربر با موفقیت یافت شد.';
                $status = 1;
            }
        }
        $brand = new \App\Http\Controllers\Api\BrandController;
        $data = [
            'status' => $status,
            'profile' => $profile,
            'About' => $About,
            'TypeModel' => [
                'type_advertise' => Category::where('type',Category::TYPE_ADVERTISE)->get(['id', 'title','status']),
                'type_products' => Category::where('type',Category::TYPE_PRODUCT)->get(['id', 'title','status']),
                'type_brand' => $brand->GetBrandsWithImages(),
               // 'type_piece' => ['رند','عادی'],
            ],
            'message' => $message
        ];


        return $data;
            // \App\Models\UserLogin::create([
            //     'user_agent' => \Request::header('User-Agent'),
            //     'user_ip' => \Request::ip(),
            //     'user_id' => \Auth::id()
            //     ]);
            // $request->session()->forget('faild_login');
            // return redirect()->intended('/');
    }

    public function update(Request $request){

        session_start();

    if ($_SESSION['loggedin']) {

        User::where('id',$request->id)->update($request->except('insertnewaddress','address','province','city','lable','postal_code','phone','sabet_phone','display_name','latitude','longitude','status','user_id'));


        if($request['insertnewaddress'] == 'true'){
            $address = \App\Models\Address::create([
                'province' => $request['province'],
                'city' => $request['city'],
                'address' => $request['address'],
                'lable' => $request['lable'],
                'postal_code' => $request['postal_code'],
                'phone' => $request['phone'],
                'sabet_phone' => $request['sabet_phone'],
                'display_name' => $request['display_name'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'status' => 1,
                'user_id' => $request['id'],
            ]);
        }

        $data['status'] = 1;
        $data['message'] = "Done";
    }else{
        $data['status'] = 0;
        $data['message'] = "you dont have premission";
    }

        return ($data);
    }

    public function changeUserPassword(Request $request){

        $user = User::where('id', $request['user_id'])->first();
        $user->password =  bcrypt($request['password']);

        $user->save();

        $data['status'] = 1;
        $data['message'] = "Done";

        return $data;



    }

    public function setuserprofilephoto(Request $request){
        $user = User::where('id', $request['user_id'])->first();
        $user['user_id'] = $user->id;
        \App\Http\Controllers\ImageController::uploadfromstring($request['string'],$user);
        $data['url'] = url('/').$user->avatar->src100;

        $data['status'] = 1;
        $data['message'] = "Done";
        return $data;

    }

    public function setusershopphoto(Request $request){
        $user = User::where('id', $request['user_id'])->first();
        $user['user_id'] = $user->id;
        \App\Http\Controllers\ImageController::uploadfromstring($request['string'],$user,'shop');
        $data['url'] = url('/').$user->shop_image();
        $data['status'] = 1;
        $data['message'] = "Done";
        return $data;

    }

    public function setuservisitphoto(Request $request){
        $user = User::where('id', $request['user_id'])->first();
        $user['user_id'] = $user->id;
        \App\Http\Controllers\ImageController::uploadfromstring($request['string'],$user, 'visit');
        $data['url'] = url('/').$user->visit_image();
        $data['status'] = 1;
        $data['message'] = "Done";
        return $data;

    }

    public function setuserjavazphoto(Request $request){
        $user = User::where('id', $request['user_id'])->first();
        $user['user_id'] = $user->id;
        \App\Http\Controllers\ImageController::uploadfromstring($request['string'],$user, 'javaz');
        $data['url'] = url('/').$user->javaz_image();
        $data['status'] = 1;
        $data['message'] = "Done";
        return $data;

    }

    public function userLikedProducts(Request $request){

        $where = ['user_id'=>$request['user_id'], 'type'=> 1];
        $liked_products = Like::where($where)->get();

        foreach($liked_products as $key => $liked){

            $data['products'][$key] = $liked->product;
            $data['products'][$key]['photo'] = url('/').$liked->product->base_image();

        }

        //$data['products'] = $liked_products;
        $data['status'] = 1;
        $data['message'] = "done";

        return $data;

    }
}
