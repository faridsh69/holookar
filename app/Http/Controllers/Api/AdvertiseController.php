<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Advertise;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;
use Carbon\Carbon;


class AdvertiseController extends Controller
{
    public function getallcategories()
    {


    	$data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $Advertises = Category::where('type',Category::TYPE_ADVERTISE)->get();
        foreach($Advertises as $Advertise)
        {
            $data[] = [
                'id' => $Advertise->id,
                'title' => $Advertise->title,
                'description' => $Advertise->description,
                'photo' => null,
                'active' => $Advertise->status
            ];

        }
        $product_cast = Category::where('type',Category::TYPE_PRODUCT )->get();
        foreach($product_cast as $cat)
        {
            $data[] = [
                'id' => $cat->id,
                'title' => $cat->title,
                'description' => $cat->description,
                'photo' => null,
                'active' => $cat->status
            ];

        }
        $data['status'] = 1;
        $data['message'] = "Done";
        return $data;
    }

    public function show($id)
    {
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $advertise = Advertise::find($id);





        if($advertise)
        {
            $image_src = null;
            $i=0;
            foreach($advertise->images as $key=>$image) {

                $image_src[++$i] = url('/') . $image->src;

            }
            $data[] = [
                'id' => $advertise->id,
                'title' => $advertise->title,
                'price' => $advertise->price,
                'price_type' => $advertise->price_type_translate(),
                'status' => $advertise->status_translate(),
                'images' => $image_src,
                'description' => $advertise->description,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function showcategory($category)
    {

        //die($id);
        $data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $where =[
            'type' => Category::TYPE_ADVERTISE,
            'id' => 27
        ];
//        $Advertise = Category::where('id',$category)->first();
//        if($Advertise)
//        {
//            $data['information'] = [
//                'id' => $Advertise->id,
//                'title' => $Advertise->title,
//                'description' => $Advertise->description,
//            ];
//        }
        $data['advertises'] = NULL;
        $Advertises = Advertise::where('category_id',$category)->get();
        if($Advertises != ""){
            foreach($Advertises as $ad)
                {
                    $image_src = null;
                    $i=0;
                    foreach($ad->images as $key=>$image) {

                        $image_src = url('/') . $image->src;
                        break;

                    }
                    $data['advertises'][] = [
                        'id' => $ad->id,
                        'title' => $ad->title,
                        'price' => $ad->price,
                        'image' => $image_src ? $image_src : NULL,
                    ];
                }

        }
        else {
        }

        $data['status'] = 1;
        $data['message'] = "Done!";
        return $data;
    }

    public function newest(Request $request){
        $Advertises = Advertise::orderBy('id', 'desc')->take(10)->get();

        foreach($Advertises as $ads){
            $image_src = null;
            foreach($ads->images as $image) {
                $image_src = url('/') . $image->src;
                break;

            }
            $data['advertises'][] = [
                'id'=> $ads['id'],
                'title'=> $ads['title'],
                'price'=> $ads['price'],
                'image' => $image_src ? $image_src : NULL,

            ];
        }
        $data['status'] = 1;
        $data['message'] = "Done!";
        return $data;

    }

    public function getUserAds(Request $request){



        $out = [];
        $advertises = Advertise::where('user_id', $request['user_id'])->get();

        foreach($advertises as $key => $advertise ) {

            $out[$key]['title'] = $advertise['title'];
            $out[$key]['id'] = $advertise['id'];
            $out[$key]['brand'] = $advertise->brand['title'];
            $out[$key]['category'] = $advertise->category['title'];
            $out[$key]['price'] = $advertise['price'];

            $date = Carbon::parse($advertise->created_at);
            $out[$key]['date'] = $date->timestamp;

            $out[$key]['photo'] = url('/') . $advertise->gallery_first_image();
            $out[$key]['province'] = $advertise['province'];
            $out[$key]['city'] = $advertise['city'];

        }




//        $advertises = Advertise::where('user_id', $request['user_id'])->get();
//
//        $data['advertises'] = NULL;
//        if($advertises != ""){
//            foreach($advertises as $ad)
//            {
//                $image_src = null;
//                foreach($ad->images as $image) {
//                    $ad->photo = url('/') . $image->src;
//                }
//            }
//        }
//        else {
//
//        }

        $data['advertises'] = $out;
        $data['status'] = 1;
        $data['count'] = count($out);
        $data['message'] = "Done!";
        return $data;
    }

    public function createadvertise(Request $request){
        $advertise = new Advertise();


        $advertise->title = $request['title'];
        $advertise->description = $request['description'];
        $advertise->phone = $request['phone'];
        $advertise->address = $request['address'];
        $advertise->price_type = $request['price_type'];
        $advertise->price = $request['price'];
        $advertise->noe_ghete = $request['noe_ghete'];
        $advertise->operator = $request['operator'];
        $advertise->sim_cart_type = $request['sim_cart_type'];
        $advertise->sim_cart_number = $request['sim_cart_number'];
        $advertise->aggrement = $request['aggrement'];
        $advertise->status = $request['status'];
        $advertise->admin_seen = $request['admin_seen'];
        //$advertise->image_id = $request['image_id'];
        $advertise->category_id = $request['category_id'];
        $advertise->brand_id = $request['brand_id'];
        $advertise->user_id = $request['user_id'];
        $advertise->city = $request['city'];
        $advertise->province = $request['province'];
        $advertise->lat = $request['lat'];
        $advertise->lng = $request['lng'];


        $pics[] =\App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string'],$advertise,'advertise');

        if($request->has('photo_string2') && $request['photo_string2']!=""  ){
            $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string2'],$advertise,'advertise');
        }

        if($request->has('photo_string3') && $request['photo_string3']!="" ){
            $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string3'],$advertise,'advertise');
        }

        if($request->has('photo_string4') && $request['photo_string4']!="" ){
            $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string4'],$advertise,'advertise');
        }

        $advertise->save();

        foreach($pics as $key => $value)
        {
                $advertise->images()->syncWithoutDetaching([$value['image_id']]);
        }

        $data['status'] = 1;
        $data['message'] = "Done!";
        return $data;

    }

    public function createMultiAdvertise(Request $request){



        //dd($request);


        $price = explode(';',$request['price']);
        $price_type = explode(';',$request['price_type']);
        $operator = explode(';',$request['operator']);
        $sim_cart_type = explode(';',$request['sim_cart_type']);
        $sim_cart_number = explode(';',$request['sim_cart_number']);
        $noe_ghete = explode(';',$request['noe_ghete']);
        $brands = explode(';',$request['brand_id']);



        //echo ($count);

        $request_type = $request['request_type'];



        foreach (explode(';',$request['price']) as $key=>$row) {

            $advertise = new Advertise();
            $advertise->title = $request['title'];
            $advertise->description = $request['description'];
            $advertise->phone = $request['phone'];
            $advertise->address = $request['address'];




            if($request_type == 'simcard'){
                $advertise->operator = $operator[$key];
                $advertise->sim_cart_type = $sim_cart_type[$key];
                $advertise->sim_cart_number = $sim_cart_number[$key];
                $advertise->price_type = $price_type[$key];
                $advertise->price = $price[$key];

            }else{
                $advertise->price_type = $price_type[$key];
                $advertise->price = $price[$key];
                $advertise->noe_ghete = $noe_ghete[$key];
                $advertise->brand_id = $brands[$key];

            }





            $advertise->aggrement = $request['aggrement'];
            $advertise->status = $request['status'];
            $advertise->admin_seen = $request['admin_seen'];
            //$advertise->image_id = $request['image_id'];
            $advertise->category_id = $request['category_id'];
            $advertise->user_id = $request['user_id'];
            $advertise->city = $request['city'];
            $advertise->province = $request['province'];
            $advertise->lat = $request['lat'];
            $advertise->lng = $request['lng'];

            $pics=[];

            $pics[] =\App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string'],$advertise,'advertise');

            if($request->has('photo_string2') && $request['photo_string2']!="" ){
                $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string2'],$advertise,'advertise');
            }

            if($request->has('photo_string3')  && $request['photo_string3']!=""){
                $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string3'],$advertise,'advertise');
            }

            if($request->has('photo_string4')  && $request['photo_string4']!=""){
                $pics[]  = \App\Http\Controllers\ImageController::uploadfromstringForAds($request['photo_string4'],$advertise,'advertise');
            }



            $advertise->save();

            foreach($pics as $key => $value)
            {
                $advertise->images()->syncWithoutDetaching([$value['image_id']]);
            }



        }



        $data['status'] = 1;
        $data['message'] = "Done!";
        return $data;

    }

    public function editAdvertise(Request $request){

    }

}
