<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Brand;
use \App\Models\Like;
use \App\Http\Requests\StoreProduct;

class ProductController extends Controller
{
    public function index()
    {
       //  "total": 50,
       // "per_page": 15,
       // "current_page": 1,
       // "last_page": 4,
       // "first_page_url": "http://laravel.app?page=1",
       // "last_page_url": "http://laravel.app?page=4",
       // "next_page_url": "http://laravel.app?page=2",
       // "prev_page_url": null,
       // "path": "http://laravel.app",
       // "from": 1,
       // "to": 15,
       
    	$data = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $products = Product::paginate(6);
        foreach($products as $product)
        {
            $data[] = [
                'id' => $product->id,
                'title' => $product->title,
                'image_url' => $product->base_image(),
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'inventory' => $product->inventory,
                'description' => $product->description,
                'status' => $product->status_translate(),
                'brand' => $product->brand ? $product->brand->title: '',
                'category' => $product->category ? $product->category->title : '',
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function show($id)
    {
        $data = null;
        $product_images = null;
        $message = 'خطا رخ داده است.';
        $status = 0;
        $product = Product::find($id);

        $images  = $product->images;
        foreach ($images as $image){
            $product_images[]=[
                'id' => $image->id,
                'image_url' => url('/').$image->src
            ];
        }
        //dd($product);die(X);
        if($product)
        {
            $data = [
                'id' => $product->id,
                'title' => $product->title,
                'images' => $product_images,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'inventory' => $product->inventory,
                'description' => strip_tags($product->description),
                'status' => $product->status_translate(),
                'brand' => $product->brand ? $product->brand->title: '',
                'category' => $product->category ? $product->category->title : '',
                //'features' => $product->features ? $product->features : null,
            ];

            foreach($product->features as $key=>$feature){
                $data['features'][$key]['id'] = $feature->id;
                $data['features'][$key]['title'] = $feature->title;
                $data['features'][$key]['data'] = $feature->pivot->data;
                $data['features'][$key]['status'] = $feature->status;

            }
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function newest(Request $request){
        $products = Product::orderBy('id', 'desc')->take(10)->get();
        //dd($products);
        foreach($products as $product){
            $data['advertises'][] = [
                'id'=> $product['id'],
                'title'=> $product['title'],
                'price'=> $product['price'],
                'discount'=> $product['discount_price'],
                'image'=> url('/').$product->base_image(),

            ];
        }
        $data['status'] = 1;
        $data['message'] = "Done!";
        return $data;

    }

    public function bestsellers(Request $request){ //die("A");

        $data = DB::table('factor_product')
            ->select( DB::raw('count(product_id) as product_count, product_id, sum(count) as total_sales') )
            ->groupBy('product_id')
            ->get();

        foreach ($data as $item){
            //$item->id = $item->id;
            $item->title = Product::find($item->product_id)->title;
            $item->image = url('/').Product::find($item->product_id)->base_image();
            $item->id = Product::find($item->product_id)->id;
            $item->discount = Product::find($item->product_id)->discount_price;
            $item->price = Product::find($item->product_id)->price;
        }
        $datax['status'] = 1;
        $datax['message'] = "Done!";
        $datax['advertises'] = $data;
        return $datax;

    }

    public function add_likes_to_products(Request $request){

        $where = ['user_id' => $request['user_id'], 'product_id' => $request['product_id']];

        if (Like::where($where)->exists()) {
            $data['status'] = 1;
            $data['message'] = 'you have had liked this before';
        }
        else{
            $insert = ['user_id' => $request['user_id'], 'product_id' => $request['product_id'],'type'=>$request['type'], 'user_ip'=>'-'];

            Like::create($insert);
            $data['status'] = 1;
            $data['message'] = 'you liked this product';
        }

        return $data;

    }


    public function remove_likes_from_products(Request $request){

        $where = ['user_id' => $request['user_id'], 'product_id' => $request['product_id']];

        $data['query'] = Like::where($where)->delete();
        $data['status'] = 1;
        $data['message'] = 'done';


        return $data;

    }


}
