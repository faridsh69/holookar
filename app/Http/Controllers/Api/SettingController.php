<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Models\Setting;
use \App\Http\Requests\StoreProduct;

class SettingController extends Controller
{
    public function index()
    {
    	$data = [];
        $message = 'خطا رخ داده است.';
        $status = 0;
        $settings = Setting::get();
        foreach($settings as $setting)
        {
            $data[] = [
                'key' => $setting->key,
                'value' => $setting->value,
                'description' => $setting->description,
            ];
            $message = 'با موفقیت انجام شد';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ]);
    }
}
