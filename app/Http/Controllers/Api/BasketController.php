<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Basket;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Article;
use \App\Http\Requests\StoreProduct;
use Illuminate\Queue\RedisQueue;

class BasketController extends Controller
{
    /*public function getInit()
    {
        $categories = Category::select('id','title')->where('type',Category::TYPE_PRODUCT)->get();
        $vue_products = $this->_getProductsVue();
        return [
            'categories' => $categories,
            'products' => $vue_products,
            'basket' => $this->_getUserBasketVue(),
            'total_price' => $this->_getTotalPrice(),
            'max_price' =>  collect($vue_products)->max('price'),
        ]; 
    } 

    private function _getProductsVue()
    {
        $products = Product::select('id','title','price','discount_price')
            ->orderBy('id', 'desc')
            // ->with(['images' => function ($query) {
            //     $query->first();
            // }])
            ->with('images')
            ->get();
        $vue_products = [];
        foreach($products as $product)
        {
            $vue_products[] = [
                'id' => $product->id,
                'title' => $product->title,
                'price' => $product->price,
                'discount_price' => $product->discount_price,
                'image_url' => $product->base_image(),
            ];
        }
        return $vue_products;
    }

    private function _getUserBasketVue()
    {
        $basket = $this->_getUserBasket();
        $vue_basket = [];
        foreach($basket->products as $basket_product)
        {
            $vue_basket[] = [
                'id' => $basket_product->id,
                'title' => $basket_product->title,
                'price' =>  $basket_product->price,
                'discount_price' =>  $basket_product->discount_price,
                'count' =>  $basket_product->pivot->count,
            ];
        }
        return $vue_basket;
    }

    private function _getUserBasket()
    {
        if( isset($_COOKIE['basket_id']) )
        {
            $basket_id = $_COOKIE['basket_id'];
        }else{
            $basket_id = null;
        }
        if($basket_id) {
            $basket = Basket::find($basket_id);
        }else{
            $basket = $this->_create_basket();
        }
        return $basket;
    }

    private function _create_basket()
    {
        $basket = new Basket();
        $basket->status = Basket::STATUS_ACTIVE;
        $basket->user_id = \Auth::id();
        $basket->save();
        setcookie('basket_id', $basket->id, time() + (86400 * 30), "/"); // 86400 = 1 day
        return $basket;
    }

    private function _getTotalPrice()
    {
        $basket = $this->_getUserBasket();
        $basket_product = $basket->products()->get();
        $total_price = 0;
        foreach($basket_product as $item)
        {
            if($item->discount_price){
                $total_price = $total_price + ( $item->pivot->count * $item->discount_price );
            }else{
                $total_price = $total_price + ( $item->pivot->count * $item->price );
            }
        }
        return $total_price;
    }

    public function postBasketAdd(Request $request)
    {
        $product_id = $request->product_id;
        $add = $request->add;
        $this->_addToBasket($product_id, $add);
        $vue_basket = $this->_getUserBasketVue();
        $total_price = $this->_getTotalPrice();
         return [
            'basket' => $vue_basket,
            'total_price' => $total_price,
        ];   
    }

    private function _addToBasket($product_id, $add)
    {
        $basket = $this->_getUserBasket();
        $basket_product = $basket->products()->where('product_id', $product_id);
            // ->where('inventory', '>', 0);
        if($basket_product->count() >= 1){
            $count = $basket_product->first()->pivot->count + $add;
            if($count == 0 && $add == -1){
                $basket->products()->detach([$product_id]);
            }else{
                $basket->products()->syncWithoutDetaching([$product_id => ['count' => $count ]], false);
            }
        }
        else{
            $basket->products()->syncWithoutDetaching([$product_id => ['count' => 1 ]]);
        }
    }
}
*/


//    public function addToBasket($product_id, $add, $user_id){
//        $basket = self::_getUserBasket($user_id);
//        $basket_product = $basket->products()->where('product_id', $product_id);
//        // ->where('inventory', '>', 0);
//        if($basket_product->count() >= 1){
//            $count = $basket_product->first()->pivot->count + $add;
//            if($count == 0 && $add == -1){
//                $basket->products()->detach([$product_id]);
//            }else{
//                $basket->products()->syncWithoutDetaching([$product_id => ['count' => $count ]], false);
//            }
//        }
//        else{
//            $basket->products()->syncWithoutDetaching([$product_id => ['count' => 1 ]]);
//        }
//    }


    public  function _changeApiCountBasket(Request $request)
    {

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $count = $request->count;
        $basket = $this->_get_Api_UserBasket($user_id);
        if($count == 0){
            $data['status'] = 1;
            $data['message'] = "محصول با موفقیت از سبد حذف شد";
            $data['basket_changes'] = $basket->products()->detach([$product_id]);
            $data['basket'] = $this->_getUserBasketVueProductsApi($user_id);
            return $data;
        }else{
            $data['status'] = 1;
            $data['message'] = "محصول با موفقیت به سبد اضافه شد";
            $data['basket_changes'] = $basket->products()->syncWithoutDetaching([$product_id => ['count' => $count ]]);
            $data['basket'] = $this->_getUserBasketVueProductsApi($user_id);
            return $data;
        }
    }

    public function _get_Api_UserBasket($user_id = 0, Request $request = NULL) {
        if ($user_id == 0) $user_id = $request['user_id']; //die($user_id);
        $basket = Basket::where('user_id',$user_id)
            ->orderBy('id', 'desc')
            ->first();
        if($basket){

            return $basket;
        }
        else{
           return $this->_create_Api_basket($user_id);
        }
    }


    public function _get_Api_UserBasketContents(Request $request) { //die($user_id);
        $basket = Basket::where('user_id',$request['user_id'])
            ->orderBy('id', 'desc')
            ->first();
        if($basket){
            $data['status'] = 1;
            $data['message'] = "basket is exists";
            $data['basket'] = $this->_getUserBasketVueProductsApi($request['user_id']);
            return $data;
        }
        else{
            $data['status'] = 0;
            $data['message'] = "basket is empty";
            $data['basket'] = NULL;
            return $data;
        }
    }


    public function _create_Api_basket($user_id){
        $basket = new Basket();
        $basket->status = Basket::STATUS_ACTIVE;
        $basket->user_id = $user_id;
        $basket->save();

//        setcookie('basket_id', $basket->id, time() + (86400 * 3), "/"); // 86400 = 1 day
//        session(['basket_id' => $basket->id]);

        return $basket;
    }

    public function _getUserBasketVueProductsApi($user_id)
    {
        $basket = $this->_get_Api_UserBasket($user_id);
        $vue_basket = [];
        foreach($basket->products as $basket_product)
        {
            $vue_basket[] = [
                'id' => $basket_product->id,
                'title' => $basket_product->title,
                'price' =>  $basket_product->price,
                'discount_price' =>  $basket_product->discount_price,
                'count' =>  $basket_product->pivot->count,
                'image_url' => url('/').$basket_product->base_image(),
            ];
        }
        return $vue_basket;
    }
}
