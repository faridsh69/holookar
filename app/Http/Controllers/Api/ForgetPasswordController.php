<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use \App\Models\Category;
use \App\Models\User;

class ForgetPasswordController extends Controller
{
    public function postPhone(Request $request)
    {

       // die("AAAA");
        $message = 'شماره تلفن در سیستم وجود ندارد.';
        $status = 0;
        $user = User::where('phone', $request['phone'])->first();

        if($user)
        {
            $code = round((intval($user->id)) * strtotime(date("Ymd")) / 98765);
           // die($code);
            $content = 'کد شما '. $code .'می باشد.';
            \App\Http\Controllers\SmsController::send_sms($user, $content);
            $status = 1;
            $message = 'کد ارسال شد.';
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
            'code' => $code,
        ]);
    }

    public function postPhoneCode(Request $request)
    {
        $message = 'کد مورد نظر اشتباه است.';
        $status = 0;
        $user = User::where('phone', $request['phone'])->first();

        if($user)
        {
            $code = round((intval($user->id)) * strtotime(date("Ymd")) / 98765);
            if($code == $request['code'])
            {
                $status = 1;
                $message = 'کد صحیح است.';
            }            
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
        ]);
    }

    public function postPhonePassword(Request $request)
    {
        $status = 0;
        $message = 'مشکلی رخ داده است.';
        $user = User::where('phone', $request['phone'])->first();

        if( $user ){


            //print_r($user);die("AAA");
            $user->password = bcrypt($request['password']);
            //$user->first_name = "Changed Pass";
            $user->save();
            $message = 'با موفقیت انجام شد.';
            $status = 1;
        }
        return json_encode([
            'status' => $status,
            'message' => $message,
        ]);
    }
}
