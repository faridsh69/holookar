<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Image;
use Illuminate\Support\Facades\DB;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\User;
use \App\Models\Agent;
use \App\Models\Advertise;
use \App\Models\Brand;
use \App\Http\Requests\StoreProduct;



class AgentsController extends Controller
{

    public function index(Request $request){

        if(isset($request['brand_id']) && $request['brand_id'] !=null) {

            $agents = Agent::where('brand_id', $request['brand_id'])->get();
        }
        else{
            $agents = Agent::get();

        }

        $data['message'] = "done";
        $data['status'] = 1;
        //$data['agents'] = $agents;

        foreach($agents as $key=>$agent){
            $data['agent'][$key]['name'] = $agent->address->display_name;
            $data['agent'][$key]['address'] = $agent->address->address;
            $data['agent'][$key]['city'] = $agent->address->city;
            $data['agent'][$key]['province'] = \Config::get('constants.provinces')[$agent->address->province];
            $data['agent'][$key]['image'] = $agent->image->src100 ? url('/') . $agent->image->src100 : NULL ;
            $data['agent'][$key]['mobile'] = $agent->address->phone;
            $data['agent'][$key]['phone'] = $agent->address->sabet_phone;
            $data['agent'][$key]['brand'] = $agent->brand->title;
        }

        return $data;
}

    public function filter(Request $request){

        $agents = array();

        if(isset($request['province']) && $request['province'] !=null){
            $agents = Agent::where('brand_id', $request['brand_id'])
                ->with('Address')
                ->wherehas('address', function($query)use($request){
                    $query->where('province', '=', $request['province']);
                })
                ->get();
        }

        if(isset($request['city']) && $request['city'] !=null){
            $agents = Agent::where('brand_id', $request['brand_id'])
                ->with('Address')
                ->wherehas('address', function($query)use($request){
                    $query->where('province', '=', $request['province']);
                })
                ->wherehas('address', function($query)use($request){
                    $query->where('city', 'like', '%' . $request['city'] . '%');
                })
                ->get();
        }





//        $agents = Agent::with('Address')
//            ->wherehas('address', function($query)use($request){
//                $query->where('province', '=', $request['province']);
//            })
//            ->wherehas('address', function($query)use($request){
//                $query->where('city', 'like', '%' . $request['city'] . '%');
//            })
//            ->get();

        $data['message'] = "done";
        $data['status'] = 1;
        //$data['agents'] = $agents;

        foreach($agents as $key=>$agent){
            $data['agent'][$key]['name'] = $agent->address->display_name;
            $data['agent'][$key]['address'] = $agent->address->address;
            $data['agent'][$key]['city'] = $agent->address->city;
            $data['agent'][$key]['province'] = \Config::get('constants.provinces')[$agent->address->province];
            $data['agent'][$key]['image'] = $agent->image->src100 ? url('/') . $agent->image->src100 : NULL ;
            $data['agent'][$key]['mobile'] = $agent->address->phone;
            $data['agent'][$key]['phone'] = $agent->address->sabet_phone;
            $data['agent'][$key]['brand'] = $agent->brand->title;
        }

        return $data;
    }


}

