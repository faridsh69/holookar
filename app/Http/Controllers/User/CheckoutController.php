<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Basket;
use App\Models\Factor;
use App\Models\Feature;
use App\Models\ProductView;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\Tagend;
use SoapClient;
use Carbon\carbon;

class CheckoutController extends Controller
{
    public function getAddress()
    {
        $basket = self::_getUserBasket();
        return view('user.checkout.address', compact('basket') );
    }

    public function getAddressInit()
    {
        $addresses = \Auth::user()->addresses()->orderBy('id', 'desc')->get()->toArray();
    	return [
    		'addresses' =>  $addresses,
    	];
    }

    public function postAddress(Request $request)
    {
		$address_id = $request->address_id;
		$user_description = $request->user_description;

        $old_factor = Factor::currentFactor()
            ->first();
        if($old_factor)
        {
            $old_factor->address_id = $address_id;
            $old_factor->user_description = $user_description;
            $old_factor->total_price = self::_getTotalPrice();
            $old_factor->status = Factor::STATUS_INITIAL;
            $old_factor->save();
            $factor = $old_factor;
        }
        else
        {
    		$new_factor_model = [
    		    'user_id' => \Auth::id(),
                'address_id' => $address_id,
    		    'user_description' => $user_description,
                'admin_seen' => 0,
                'status' => Factor::STATUS_INITIAL,
    	        'total_price' => self::_getTotalPrice(),
            ];
            $factor = Factor::create($new_factor_model);
        }

        $basket = self::_getUserBasket();
        $factor->products()->sync([]);
        foreach ($basket->products as $product) 
        {
            // if($product->status != Product::STATUS_AVAILABLE){
            //     continue;
            // }
            $count = $product->pivot->count;
            $factor->products()->syncWithoutDetaching([
                $product->id => [
                    'count' => $count,
                    'price' =>  $product->price,
                    'discount_price' =>  $product->discount_price,
                ]
            ]);
        }

        $tagends = Tagend::where('is_copon', 0)
            ->where('title', 'NOT LIKE', '%ارسال%')
            ->Active()
            ->get();

        foreach ($tagends as $tagend) 
        {
            if($tagend->type == 0)
            { // absolute
                if( $tagend->sign == 1){
                    $value = $tagend->value;
                }else{
                    $value = (-1) * $tagend->value;
                }
            }else{ // percent
                if( $tagend->sign == 1){
                    $value = ( $tagend->value * $factor->total_price ) / 100;
                }else{
                    $value = ( (-1) * $tagend->value * $factor->total_price) / 100;
                }
            }
            $factor->tagends()->syncWithoutDetaching([
                $tagend->id => [
                    'value' => $value,
                ]
            ]);
        }

        $factor->total_price = $factor->calculateTotalPriceWithTagends();
        $factor->save();

        return json_encode([
            'status' => 1,
            'message' => 'فاکتور با موفقیت ذخیره شد.',
            'data' => $factor->id,
        ]);
    }

    public function getShipping()
    {
        $shippings = [];
        $tagends =  Tagend::get();
        foreach($tagends as $tagend){
            if( strpos($tagend->title,'ارسال') !== false){
                $shippings[] = $tagend;
            }
        }

        $factor = Factor::currentFactor()->first();

        return view('user.checkout.shipping', compact('factor', 'shippings'));
    }

    public function postShipping(Request $request)
    {
        $factor = Factor::currentFactor()->first();

        if($factor->shipping)
        {
            // detach kardane raveshe ersal agar zade bod
            $tagends = Tagend::where('is_copon', 0)
                ->where('title', 'like', '%ارسال%')
                ->Active()
                ->get();

            foreach ($tagends as $tagend) 
            {
                $factor->tagends()->detach([$tagend->id]);
            }
        }
        $tagend_shipping = Tagend::where('title', $request['shipping'])->first();
        $factor->tagends()->syncWithoutDetaching([
            $tagend_shipping->id => [
                'value' => $tagend_shipping->value,
            ]
        ]);

        $factor->shipping = $request['shipping'];
        $factor->total_price = $factor->calculateTotalPriceWithTagends();
        $factor->save();

        return redirect('/checkout/payment');
    }

    public function getPayment()
    {
        $factor = Factor::currentFactor()->first();
        if($factor){
            return view('user.checkout.payment', compact('factor'));
        }else{
            return redirect('/');
        }
    }

    public function getPaymentLocal()
    {        
        $factor = Factor::currentFactor()->first();

        if(!$factor){
            \Log::error('getPaymentLocal + factor does not exist');
            return redirect('/');
        }

        if($factor->total_price < 1000){
            $factor->total_price = 1000;
            $factor->save();
        }
        $this->_decrease_inventory();
        $factor->payment = Factor::PAYMENT_LOCAL;
        $factor->status = Factor::STATUS_PROCCESSING;
        $factor->save();
        $view_status = 'local';
            
        $basket = self::_getUserBasket();
        $basket->products()->detach();
        $basket->delete();

        return view('user.checkout.verify', compact('view_status', 'factor'));
    }

    public function getPaymentOnline()
    {
        $factor = Factor::currentFactor()->first();
        if(!$factor){
            \Log::error('getPaymentOnline + factor does not exist');
            return redirect('/');
        }

        if($factor->total_price < 100){
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        }
        $factor->payment = Factor::PAYMENT_ONLINE;
        $factor->status = Factor::STATUS_PAYMENT;
        $factor->save();

        $payment = Payment::create([
            'user_id' => \Auth::id(),
            'factor_id' => $factor->id,
            'user_ip' => \Request::ip(),
            'total_price' => $factor->total_price,
            'status' => Payment::STATUS_SEND_BANK,
            'Invoice_date' => date('now'),
            'description' => 'در حال ورود به بانک',
            'bank' => 'zarinpal',
        ]);
        \Log::info('going to bank factor_id: '.$factor->id.' by user_id: ' .\Auth::id() 
            .'with payment_id: '.$payment->id );
                
        try{
            $gateway = \Gateway::ZARINPAL();

            $gateway->setCallback(url('/checkout/payment/verify'));
            $gateway->setDescription('سفارش از فروشگاه اینترنتی');
            $gateway->setEmail(\Auth::user()->email);
            $gateway->setMobileNumber(\Auth::user()->phone);
            $gateway->price( (10 * $factor->total_price) )->ready();

            $refId =  $gateway->refId(); // شماره ارجاع بانک
            $transID = $gateway->transactionId(); // شماره تراکنش
            
            $payment->refId = $refId; // 000000000000000000000000000072078672
            $payment->transaction_id = $transID; // 152420075508
            $payment->save();
            return $gateway->redirect();
            
        } catch (\Exception $e) {
            $payment->status = Payment::STATUS_UNSUCCEED;
            $payment->error = $e->getMessage();
            $payment->description = 'اطلاعات بانک تو سیستم غلط است';
            $payment->save();
            \Log::error('اطلاعات بانک اشتباه رخ داده است.' );
            dd( $e->getMessage() );
        }
        // if(!$factor){
        //     \Log::error('getPaymentOnline + factor does not exist');
        //     return redirect('/');
        // }

        // if($factor->total_price < 1000){
        //     dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        //     \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        // }
        // $factor->payment = Factor::PAYMENT_ONLINE;
        // $factor->status = Factor::STATUS_PAYMENT;
        // $factor->save();

        // // $table->string('tref')->nullable();
        // // $table->string('payment')->nullable();
        // // $table->string('error')->nullable();

        // $payment = Payment::create([
        //     'user_id' => \Auth::id(),
        //     'factor_id' => $factor->id,
        //     'user_ip' => \Request::ip(),
        //     'total_price' => $factor->total_price,
        //     'status' => Payment::STATUS_SEND_BANK,
        //     'Invoice_date' => date('now'),
        //     'description' => 'در حال ورود به بانک',
        //     'payment' => 'zarinpal',
        // ]);
        // \Log::info('going to bank factor_id: '.$factor->id.' by user_id: ' .\Auth::id() 
        //     .'with payment_id: '.$payment->id );
        
        // $settings = Setting::pluck('value','key');
        // $MerchantID = $settings['zarin_pal'];
        // $Amount = $factor->total_price; 
        // $Description = 'سفارش از ' . $settings['name'];  
        // $Email = \Auth::user()->email; 
        // $Mobile = \Auth::user()->phone; 
        // $CallbackURL = url('/checkout/payment/verify'); 
          
        // // URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
        // $client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
          
        // $result = $client->PaymentRequest(
        //     array(
        //         'MerchantID'   => $MerchantID,
        //         'Amount'   => $Amount,
        //         'Description'   => $Description,
        //         'Email'   => $Email,
        //         'Mobile'   => $Mobile,
        //         'CallbackURL'   => $CallbackURL
        //     )
        // );
          
        // //Redirect to URL You can do it also by creating a form
        // if($result->Status == 100)
        // {
        //     $payment->Invoice_number = $result->Authority;
        //     $payment->save();
        //     Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);exit;
        // } else {
        //     $payment->error = $result->Status;
        //     $payment->description = 'اطلاعات بانک تو سیستم غلطه';
        //     $payment->save();
        //     dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',$result->Status);
        //     \Log::warning(' "اطلاعات بانک اشتباه" رخ داده است. ',' کد خطا: '.$result->Status );
        // }
    }

    public function getPaymentVerify()
    {
        $Authority = $_GET['Authority'];
        $payment = Payment::Mine()
            ->where('refId', $Authority)
            ->orderBy('id','desc')
            ->first();
        if(!$payment){
            dd('پرداخت به مشکل خورده است شما با این کد به بانک نرفته اید!');
        }
       
        $factor = $payment->factor;
        $Amount = $payment->total_price;

        try { 
            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();

            $payment->tracking_code = $trackingCode;
            $payment->refId = $refId;
            $payment->card_number = $cardNumber;
            $payment->description = 'با موفقیت پرداخت شد.';
            $payment->status = Payment::STATUS_SUCCEED;
            
            $factor->status = Factor::STATUS_PROCCESSING;
            $view_status = 'success';
            $this->_decrease_inventory();
            // vase saheb maghaze sms bezan

        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {
            if($payment->status == Payment::STATUS_SUCCEED){
                $view_status = 'success';
            }else{
                $view_status = 'error';
            }
            return view('user.checkout.verify', compact('view_status', 'factor') );

        } catch (\Exception $e) {
            \Log::warning('user canceled: by payment_id: '. $payment->id );
            $payment->status = Payment::STATUS_UNSUCCEED;
            $payment->error = $e->getMessage();
            $payment->description = 'پرداخت ناموفق بوده است';
            $view_status = 'error';
        }
        $payment->save();
        $factor->save();
        
        $basket = self::_getUserBasket();
        $basket->products()->detach();
        $basket->delete();
        
        return view('user.checkout.verify', compact('view_status', 'factor') );

        
        // $Authority = $_GET['Authority'];
        // $payment = \App\Models\Payment::Mine()
        //     ->where('Invoice_number', (int)$Authority)
        //     ->orderBy('id','desc')
        //     ->first();

        // if(!$payment)
        //     dd('پرداخت به مشکل خوردده است!');
        // $factor = $payment->factor;
        // $settings = Setting::pluck('value','key');
        // $MerchantID = $settings['zarin_pal'];
        // $Amount = $payment->total_price;

        // if ($_GET['Status'] == 'OK') {
        //     $client = new \SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl',array('encoding' => 'UTF-8')); 
        //     $result = $client->PaymentVerification(
        //         [
        //         'MerchantID' => $MerchantID,
        //         'Authority' => $Authority,
        //         'Amount'   => $Amount,
        //         ]
        //     );
        //     if ($result->Status == 100) {
        //         \Log::info('payment good done : by payment_id: '. $payment->id . ' invN : ' .$result->RefID);
        //         $view_status = 'success';
        //         $factor->status = Factor::STATUS_PROCCESSING;
        //         $payment->description = $result->RefID;
        //         $payment->status = Payment::STATUS_SUCCEED;
        //         $payment->Invoice_number = (int)$Authority;
                
        //         // vase saheb maghaze sms bezan
        //         // inja mojodi o kam kon TODO
        //     } else {
        //         \Log::warning('payment has error : by payment_id: '. $payment->id . ' status : ' .$result->Status);
        //         $view_status = 'error';
        //         $payment->Invoice_number = (int)$Authority;
        //         $payment->description = self::_getErrorZarrinpal($result->Status);
        //         $payment->status = Payment::STATUS_UNSUCCEED;
        //     }
        // } else {
        //     \Log::warning('user canceled: by payment_id: '. $payment->id );
        //     $view_status = 'error';
        //     $payment->Invoice_number = (int)$Authority;
        //     $payment->description = 'کاربر کنسل کرده';
        //     $payment->status = Payment::STATUS_UNSUCCEED;
        // }
        // $this->_decrease_inventory();
        // $payment->save();
        // $factor->save();
        
        // $basket = self::_getUserBasket();
        // $basket->products()->detach();
        // $basket->delete();

        // return view('user.checkout.verify', compact('view_status', 'factor') );
    }

    private function _decrease_inventory()
    {
        try {
            $factor = Factor::currentFactor()->first();

            foreach($factor->products as $product)
            {
                $product->inventory = $product->inventory - $product->pivot->count; 
            }
        }
        catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
        }
    }
}
