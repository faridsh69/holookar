<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use SoapClient;
// use App\Notifications\KharidKonande;
// use App\Notifications\ForgetPassword;
use App\Models\Product;
use App\Models\Article;
use App\Models\News;
use App\Models\Brand;
use App\Models\Baner;
use App\Models\User;
use App\Models\Factor;
use App\Models\Payment;

class MasterController extends Controller
{
    // public function getUsers()
    // {
        
    // }
    
	public function index() 
    {
    	$new_products = User::where('is_holookar', 1)->take(30)->get();
    	$discount_products = Product::whereNull('discount_price')->Mojod()->Desc()->take(10)->get();
        $brands = Brand::Active()->get();
    	$newses = News::Active()->get();
    	$baners_right_slider = Baner::where('location', Baner::LOCATION_RIGHT_SLIDER)->get();
    	$baners_left_slider = Baner::where('location', Baner::LOCATION_LEFT_SLIDER)->get();

        return view('user.index', compact('newses', 'baners_right_slider', 'baners_left_slider', 'new_products'));
    }

    public function getLanguage($language)
    {
        if($language == 'fa' || $language == 'en'){
            session()->put('local_language', $language);
        }
        
        return redirect()->back();
    }

    public function test3()
    {
        // dd(\Auth::user()->email);
        // \Auth::user()->notify(new \App\Mail\UserLogin());
        \Mail::to(\Auth::user()->email)->send(new \App\Mail\UserLogin());

    }

    public function test()
    {
        $factor = Factor::first();
        if(!$factor){
            \Log::error('getPaymentOnline + factor does not exist');
            return redirect('/');
        }

        if($factor->total_price < 1000){
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        }
        $factor->payment = Factor::PAYMENT_ONLINE;
        $factor->status = Factor::STATUS_PAYMENT;
        $factor->save();

        $payment = Payment::create([
            'user_id' => \Auth::id(),
            'factor_id' => $factor->id,
            'user_ip' => \Request::ip(),
            'total_price' => $factor->total_price,
            'status' => Payment::STATUS_SEND_BANK,
            'Invoice_date' => date('now'),
            'description' => 'در حال ورود به بانک',
            'bank' => 'zarinpal',
        ]);
        \Log::info('going to bank factor_id: '.$factor->id.' by user_id: ' .\Auth::id() 
            .'with payment_id: '.$payment->id );
                
        try{
            $gateway = \Gateway::ZARINPAL();

            $gateway->setCallback(url('/checkout/payment/verify'));
            $gateway->setDescription('سفارش از فروشگاه اینترنتی');
            $gateway->setEmail(\Auth::user()->email);
            $gateway->setMobileNumber(\Auth::user()->phone);
            // $gateway->price( (10 * $factor->total_price) )->ready();
            $gateway->price( (1000) )->ready();

            $refId =  $gateway->refId(); // شماره ارجاع بانک
            $transID = $gateway->transactionId(); // شماره تراکنش
            
            $payment->refId = $refId; // 000000000000000000000000000072078672
            $payment->transaction_id = $transID; // 152420075508
            $payment->save();
            return $gateway->redirect();
            
        } catch (\Exception $e) {
            $payment->status = Payment::STATUS_UNSUCCEED;
            $payment->error = $e->getMessage();
            $payment->description = 'اطلاعات بانک تو سیستم غلط است';
            $payment->save();
            \Log::error('اطلاعات بانک اشتباه رخ داده است.' );
            dd( $e->getMessage() );
        }
    }

    public function test2()
    {
        $Authority = $_GET['Authority'];
        $payment = Payment::Mine()
            ->where('refId', $Authority)
            ->orderBy('id','desc')
            ->first();
        if(!$payment){
            dd('پرداخت به مشکل خورده است شما با این کد به بانک نرفته اید!');
        }
       
        $factor = $payment->factor;
        $Amount = $payment->total_price;

        try { 
            $gateway = \Gateway::verify();
            $trackingCode = $gateway->trackingCode();
            $refId = $gateway->refId();
            $cardNumber = $gateway->cardNumber();

            $payment->tracking_code = $trackingCode;
            $payment->refId = $refId;
            $payment->card_number = $cardNumber;
            $payment->description = 'با موفقیت پرداخت شد.';
            $payment->status = Payment::STATUS_SUCCEED;
            
            $factor->status = Factor::STATUS_PROCCESSING;
            $view_status = 'success';
            $this->_decrease_inventory();
            // vase saheb maghaze sms bezan

        } catch (\Larabookir\Gateway\Exceptions\RetryException $e) {
            if($payment->status == Payment::STATUS_SUCCEED){
                $view_status = 'success';
            }else{
                $view_status = 'error';
            }
            return view('user.checkout.verify', compact('view_status', 'factor') );

        } catch (\Exception $e) {
            \Log::warning('user canceled: by payment_id: '. $payment->id );
            $payment->status = Payment::STATUS_UNSUCCEED;
            $payment->error = $e->getMessage();
            $payment->description = 'پرداخت ناموفق بوده است';
            $view_status = 'error';
        }
        $payment->save();
        $factor->save();
        
        $basket = self::_getUserBasket();
        $basket->products()->detach();
        $basket->delete();
        
        return view('user.checkout.verify', compact('view_status', 'factor') );
    }

    private function _decrease_inventory()
    {
        try {
            $factor = Factor::currentFactor()->first();

            foreach($factor->products as $product)
            {
                $product->inventory = $product->inventory - $product->pivot->count; 
            }
        }
        catch(Exception $e) {
          echo 'Message: ' .$e->getMessage();
        }
    }
}

