<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisan;

class CommandController extends Controller
{
    public function configCache()
    {
        try {
            echo '<br>php artisan config:cache...';
            Artisan::call('config:cache');
            echo '<br>php artisan config:cache completed';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }

    public function dbSeed()
    {
        try {
            echo '<br>php artisan db:seed...';
            Artisan::call('db:seed');
            echo '<br>php artisan db:seed completed';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }

    public function migrate()
    {        
        try {
            echo '<br>php artisan migrate...';
            Artisan::call('migrate');
            echo '<br>php artisan migrate completed';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    }  
}
