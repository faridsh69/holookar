<?php

use Illuminate\Database\Seeder;
use App\Models\Tagend;
class TagendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagends = [
        	[
        		'title' => 'ارسال معمولی',
        		'description' => 'زمان ارسال بین ۱ الی ۲ روز است.',
        		'value' => 3000,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_ABSOLUTE,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_ACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	[
        		'title' => 'ارسال سفارشی',
        		'description' => 'زمان ارسال کمتر از ۱ روز است.',
        		'value' => 5000,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_ABSOLUTE,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_ACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	[
        		'title' => 'ارسال با پیک موتوری',
        		'description' => 'زمان ارسال تا ساعاتی دیگر',
        		'value' => 8000,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_ABSOLUTE,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_ACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	[
        		'title' => 'مالیات',
        		'description' => 'مالیات برابر ۲ درصد می باشد.',
        		'value' => 2,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_PERCENT,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_DEACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	[
        		'title' => 'ارزش افزوده',
        		'description' => 'ارزش افزوده برابر ۳ درصد می باشد.',
        		'value' => 3,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_PERCENT,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_DEACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	[
        		'title' => 'هزینه بسته بندی',
        		'description' => 'هزینه بسته بندی هزار تومان است.',
        		'value' => 1000,
        		'sign' => Tagend::SIGN_POSITIVE,
        		'type' => Tagend::TYPE_ABSOLUTE,
        		'is_copon' => 0,
        		'code' => null,
        		'status' => Tagend::STATUS_DEACTIVE,
        		'used_by' => null,
        		'user_id' => 1,
        	],
        	// [
        	// 	'title' => 'کد تخفیف 1',
        	// 	'description' => 'کد تخفیف ۲۵ درصدی',
        	// 	'value' => 25,
        	// 	'sign' => Tagend::SIGN_POSITIVE,
        	// 	'type' => Tagend::TYPE_PERCENT,
        	// 	'is_copon' => 1,
        	// 	'code' => '123456',
        	// 	'status' => Tagend::STATUS_ACTIVE,
        	// 	'used_by' => null,
        	// 	'user_id' => 1,
        	// ],
        	// [
        	// 	'title' => 'کد تخفیف 2',
        	// 	'description' => 'کد تخفیف ۹۵ درصدی',
        	// 	'value' => 95,
        	// 	'sign' => Tagend::SIGN_POSITIVE,
        	// 	'type' => Tagend::TYPE_PERCENT,
        	// 	'is_copon' => 1,
        	// 	'code' => '654321',
        	// 	'status' => Tagend::STATUS_ACTIVE,
        	// 	'used_by' => 1,
        	// 	'user_id' => 1,
        	// ],
        ];

        foreach($tagends as $tagend){
            Tagend::updateOrCreate(['title' => $tagend['title']], $tagend);
        }
    }
}
