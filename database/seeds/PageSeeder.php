<?php

use Illuminate\Database\Seeder;
use \App\Models\Page;
use \App\Models\Category;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()	
    {
        $pages = [
            [
                'title' => 'گارانتی',
                'top_title' => '',
                'sub_title' => '',
                'content' => 'گارانتی این شرکت 100 در صدر تضمینی و قطعی می باشد.',
                'status' => Page::STATUS_ACTIVE,
                // 'image_id' => 1,
                'meta_title' => 'گارانتی',
                'meta_description' => 'گارانتی',
            ],
            // [
            // 	'title' => 'دانلود اپلیکیشن',
            // 	'content' => 'برای دانلود اپلیکیشن مورد نظر می توانید در کافه بازار یا اپل استور کلمه application را جستجو نمایید',
            // 	'status' => Page::STATUS_ACTIVE,
            // 	// 'image_id' => 1,
            // 	'meta_title' => 'دانلود اپلیکیشن',
            // 	'meta_description' => 'دانلود اپلیکیشن',
            // ],
        ];
        $category = Category::where('type',Category::TYPE_PAGE)->first();

        foreach($pages as $page)
        {
        	$page['category_id'] = $category ? $category->id : null ;
        	$page['user_id'] = 2;

            Page::firstOrCreate($page);
        }
    }
}
