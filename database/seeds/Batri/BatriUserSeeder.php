<?php

use Illuminate\Database\Seeder;
use \App\Models\User;

class BatriUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =[ 
        	[
        		'id' => 1,
	        	'first_name' => 'فرید',
	    		'last_name' => 'شهیدی',
	    		'phone' => '1',
	    		'password' => bcrypt(1),
	    		'email' => 'farid.sh69@gmail.com',
	    		'national_code' => '1270739034',
	    		'gender' => 'male',
	    		'birthday' => '1990-01-10',
	    		'used_marketer_code' => '1-far',
	    		'generated_marketer_code' => '1-far',
	    		'rate' => 5,
	    		'credit' => 10000,
	    		'status' => User::STATUS_ACTIVE,
	    		'image_id' => 1,
	    	],
            ['id' => 2, 'first_name' => 'آقای', 'last_name' => 'ندایی', 'phone' => '2', 'password' => bcrypt(2)],
            ['id' => 3, 'first_name' => 'حسین', 'last_name' => 'کوپایی', 'phone' => '3', 'password' => bcrypt(3)],  
            ['id' => 4, 'first_name' => 'خانوم', 'last_name' => 'فلاحی', 'phone' => '4', 'password' => bcrypt(4)],            
        ];
        
        foreach($users as $user)
        {
            $user = User::updateOrCreate(['id' => $user['id'] ] , $user);
            if($user->id == 1 ){ 
                $user->roles()->sync([4,14], false);
            }elseif($user->id == 2 ){
                $user->roles()->sync([4,14], false);
            }
            elseif($user->id == 3 ){
                $user->roles()->sync([1,2,3,4,10], false);
            }
            elseif($user->id == 4 ){
                $user->roles()->sync([1,3,4,7,10], false);
            }
        }
    }
}
