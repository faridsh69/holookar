Holo<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class HoloSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key' => 'name',
                'value' => 'هلوکار',
                'description' => 'نام سامانه',
            ],
            [
                'key' => 'description',
                'value' => 'هلو کار سامانه یافتن متخص نرم افزار حسابداری هلو',
                'description' => 'توضیحات',
            ],
            [
                'key' => 'phone',
                'value' => '02166432432',
                'description' => 'تلفن دفتر',
            ],
            [
                'key' => 'mobile',
                'value' => '02166947263',
                'description' => 'شماره همراه',
            ],
            [
                'key' => 'email',
                'value' => 'farid.sh69@gmail.com',
                'description' => 'ایمیل',
            ],
            [
                'key' => 'fax',
                'value' => '-',
                'description' => 'فکس',
            ],
            [
                'key' => 'address',
                'value' => 'تهران- تقاطع خیابان آزادی و اسکندری پشت داروخانه بیژن - پلاک 477 - طبقه 3 - واحد 35',
                'description' => 'آدرس دفتر',
            ],
            [
                'key' => 'telegram',
                'value' => 'holookar',
                'description' => 'آدرس تلگرام',
            ],
            [
                'key' => 'instagram',
                'value' => 'holookar',
                'description' => 'آدرس اینستاگرام',
            ],
            [
                'key' => 'card_number',
                'value' => '6037-6916-6097-xxxx',
                'description' => 'شماره کارت فروشگاه',
            ],
            [
                'key' => 'account_number',
                'value' => '1302502550221xxx',
                'description' => 'شماره حساب فروشگاه',
            ],
            [
                'key' => 'merchant_code',
                'value' => '213124123',
                'description' => 'کد مشتری درگاه بانک',
            ],
            [
                'key' => 'terminal_code',
                'value' => '213124123',
                'description' => 'کد ترمینال درگاه بانک',
            ],
            [
                'key' => 'zarin_pal',
                'value' => 'ea12b276-429a-11e7-a249-005056a205be',
                'description' => 'درگاه زرین بال',
            ],
            [
                'key' => 'email_sender',
                'value' => 'farid.sh69@gmail.com',
                'description' => 'ایمیل مختص ارسالی به کاربران',
            ],
            [
                'key' => 'email_sender_password',
                'value' => 'dfxbcixyoaromzpq',
                'description' => 'رمز ایمیل مختص ارسالی به کاربران',
            ],
            [
                'key' => 'sms_user',
                'value' => 's.farid.sh69',
                'description' => 'نام کاربری پنل اس ام اس',
            ],
            [
                'key' => 'sms_pass',
                'value' => '390261',
                'description' => 'رمز عبور پنل اس ام اس',
            ],
            [
                'key' => 'sms_phone',
                'value' => '50005708616261',
                'description' => 'شماره پنل اس ام اس',
            ],
            [
                'key' => 'sms_url',
                'value' => 'http://payamak-service.ir/SendService.svc?wsdl',
                'description' => 'url server sms',
            ],
            [
                'key' => 'logo',
                'value' => '/upload/images/logo_holoo.png',
                'description' => 'لوگو',
            ],
            [
                'key' => 'default_image',
                'value' => '/upload/images/default.png',
                'description' => 'عکس پیش فرض در سیستم',
            ],
            [
                'key' => 'default_image_user',
                'value' => '/upload/images/default_user.png',
                'description' => 'عکس پیش فرض در سیستم برای کاربران',
            ],
            [
                'key' => 'default_image_product',
                'value' => '/upload/images/default_product_holoo.png',
                'description' => 'عکس پیش فرض در سیستم برای محصولات',
            ],
            [
                'key' => 'favicon',
                'value' => '/upload/images/favicon.png',
                'description' => 'لوگو روی تب',
            ],
            [
                'key' => 'enamad',
                'value' => '123124123123123',
                'description' => 'کد اعتماد الکترونیکی',
            ],
            [
                'key' => 'google_analytics',
                'value' => 'UA-97891904-1',
                'description' => 'کد گوگل آنالیتیکس',
            ],
            [
                'key' => 'crisp',
                'value' => 'UA-97891904-1',
                'description' => 'چت آنلاین',
            ],
            [
                'key' => 'latitude',
                'value' => '35.2',
                'description' => 'عرض جغرافیایی',
            ],
            [
                'key' => 'longitude',
                'value' => '53.5',
                'description' => 'طول جغرافیایی',
            ],
            [
                'key' => 'register_sms',
                'value' => 'با سلام. \n به سایت خود خوش آمدید',
                'description' => 'اس ام اس ارسالی در موقع ثبت نام',
            ],
            [
                'key' => 'register_email',
                'value' => 'با سلام. \n به سایت خود خوش آمدید',
                'description' => 'ایمیل ارسالی در موقع ثبت نام',
            ],
            [
                'key' => 'shopping_sms',
                'value' => 'سفارش شما در سیستم ثبت شد. مسولین در حال آماده سازی کالاها می باشند.',
                'description' => 'اس ام اس ارسالی در موقع ثبت سفارش',
            ],
            [
                'key' => 'shopping_email',
                'value' => 'سفارش شما در سیستم ثبت شد. مسولین در حال آماده سازی کالاها می باشند.',
                'description' => 'ایمیل ارسالی در موقع ثبت سفارش',
            ],
            [
                'key' => 'advertise_why_not_accept_status',
                'value' => "این آگهی شامل اطلاعات نادرست است.+ در این آگهی اطلاعات ناکافی است.+ این آگهی مورد تایید نیست.",
                'description' => 'دلایل رد شدن آگهی ها',
            ],
            [
                'key' => 'theme',
                'value' => 'batri',
                'description' => 'قالب انتخابی برای سایت',
            ],
        ];
        
        foreach($settings as $setting){
            Setting::updateOrCreate(['key' => $setting['key']], $setting);
        }
    }
}
