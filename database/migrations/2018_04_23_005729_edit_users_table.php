<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_holookar')->default(0);
            $table->integer('age')->nullable();
            $table->string('city')->nullable();
            $table->string('tahsilat')->nullable();
            $table->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_holookar');
            $table->dropColumn('age');
            $table->dropColumn('city');
            $table->dropColumn('tahsilat');
            $table->dropColumn('description');
        });
    }
}
